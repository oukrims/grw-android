package com.apps.items;

public class ItemVideos {

 	private String id, cid, user_name, cname, type, title, url, video_id, duration, image_big, image_small, tags, rate_avg, total_rate, total_views;

	public ItemVideos(String id, String cid, String user_name, String cname, String type, String title, String url, String video_id, String duration, String image_big, String image_small, String tags, String rate_avg, String total_views) {
		this.id = id;
		this.cid = cid;
		this.cname = cname;
		this.type = type;
		this.title = title;
		this.url = url;
		this.video_id = video_id;
		this.duration = duration;
		this.user_name = user_name;
		this.image_big = image_big;
		this.image_small = image_small;
		this.tags = tags;
		this.rate_avg = rate_avg;
		this.total_views = total_views;
	}

	public String getId() {
		return id;
	}

	public String getCId() {
		return cid;
	}

	public String getUserName() {
		return user_name;
	}

	public String getCName() {
		return cname;
	}

	public String getImageBig() {
		return image_big;
	}

	public String getImageSmall() {
		return image_small;
	}

	public String getTags() {
		return tags;
	}

	public String getRateAvg() {
		return rate_avg;
	}

	public String getTotalRate() {
		return total_rate;
	}

	public String getTotalViews() {
		return total_views;
	}

	public String getType() {
		return type;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public String getVideo_id() {
		return video_id;
	}

	public String getDuration() {
		return duration;
	}

	public void setTotalViews(String total_views) {
		this.total_views = total_views;
	}

	public void setRateAvg(String rate_avg) {
		this.rate_avg = rate_avg;
	}
}
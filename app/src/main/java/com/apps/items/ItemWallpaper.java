package com.apps.items;

public class ItemWallpaper {

 	private String id, cid, user_name, cname, image_big, image_small, tags, rate_avg, total_rate, total_views, total_download;

	public ItemWallpaper(String id, String cid, String user_name, String cname, String image_big, String image_small, String tags, String rate_avg, String total_views, String total_download) {
		this.id = id;
		this.cid = cid;
		this.cname = cname;
		this.user_name = user_name;
		this.image_big = image_big;
		this.image_small = image_small;
		this.tags = tags;
		this.rate_avg = rate_avg;
		this.total_views = total_views;
		this.total_download = total_download;
	}

	public String getId() {
		return id;
	}

	public String getCId() {
		return cid;
	}

	public String getUserName() {
		return user_name;
	}

	public String getCName() {
		return cname;
	}

	public String getImageBig() {
		return image_big;
	}

	public String getImageSmall() {
		return image_small;
	}

	public String getTags() {
		return tags;
	}

	public String getRateAvg() {
		return rate_avg;
	}

	public String getTotalRate() {
		return total_rate;
	}

	public String getTotalViews() {
		return total_views;
	}

	public String getTotalDownload() {
		return total_download;
	}

	public void setTotalViews(String total_views) {
		this.total_views = total_views;
	}

	public void setRateAvg(String rate_avg) {
		this.rate_avg = rate_avg;
	}

	public void setTotalDownloads(String total_download) {
		this.total_download = total_download;
	}
}
package com.apps.items;

public class ItemRingtone {

	private String id, cid, cname, user_name, type, title, url, duration, image_big, image_small, tags, rate_avg, total_rate, total_views, total_download;

	public ItemRingtone(String id, String cid, String cname, String user_name, String type, String title, String url, String duration, String image_big, String image_small, String tags, String rate_avg, String total_views, String total_download) {
		this.id = id;
		this.cid = cid;
		this.cname = cname;
		this.user_name = user_name;
		this.type = type;
		this.title = title;
		this.url = url;
		this.duration = duration;
		this.image_big = image_big;
		this.image_small = image_small;
		this.tags = tags;
		this.rate_avg = rate_avg;
		this.total_views = total_views;
		this.total_download = total_download;
	}

	public String getId() {
		return id;
	}

	public String getCId() {
		return cid;
	}

	public String getCname() {
		return cname;
	}

	public String getUserName() {
		return user_name;
	}

	public String getImageBig() {
		return image_big;
	}

	public String getImageSmall() {
		return image_small;
	}

	public String getTags() {
		return tags;
	}

	public String getRateAvg() {
		return rate_avg;
	}

	public String getTotalRate() {
		return total_rate;
	}

	public String getTotalViews() {
		return total_views;
	}

	public String getTotalDownload() {
		return total_download;
	}

	public String getType() {
		return type;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public String getDuration() {
		return duration;
	}

	public void setTotalViews(String total_views) {
		this.total_views = total_views;
	}

	public void setRateAvg(String rate_avg) {
		this.rate_avg = rate_avg;
	}

	public void setTotalDownloads(String total_download) {
		this.total_download = total_download;
	}
}
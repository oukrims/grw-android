package com.apps.items;

import java.io.Serializable;

public class UserItem implements Serializable {

    private String id, name, email, mobile, city, address;

    public UserItem(String id, String name, String email, String mobile, String city, String address)
    {
        this.id = id;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.city = city;
        this.address = address;
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getEmail()
    {
        return email;
    }

    public String getMobile()
    {
        return mobile;
    }

    public String getCity()
    {
        return city;
    }

    public String getAddress()
    {
        return address;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

}

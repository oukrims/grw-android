package com.apps.items;

public class ItemCat {

 	private String id, name, image_big, image_small;

	public ItemCat(String id, String name, String image_big, String image_small) {
		this.id = id;
		this.name = name;
		this.image_big = image_big;
		this.image_small = image_small;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getImageBig() {
		return image_big;
	}

	public String getImageSmall() {
		return image_small;
	}
}
package com.apps.utils;

import com.apps.items.ItemAbout;
import com.apps.items.ItemGIF;
import com.apps.items.ItemRingtone;
import com.apps.items.ItemVideos;
import com.apps.items.ItemWallpaper;
import com.apps.items.UserItem;

import java.io.Serializable;
import java.util.ArrayList;

public class Constant implements Serializable{

	public static String SERVER_URL = "http://138.68.53.202/rw-production/";

//	public static final String URL_IMAGE = SERVER_URL + "images/";
	public static final String URL_HOME = SERVER_URL +"api.php?home";
	public static final String URL_CAT_WALL = SERVER_URL +"api.php?wall_cat_list";
	public static final String URL_CAT_RING = SERVER_URL +"api.php?ringtone_cat_list";
	public static final String URL_CAT_VIDEOS = SERVER_URL +"api.php?video_cat_list";
	public static final String URL_WALL_BY_CAT = SERVER_URL +"api.php?wall_cat_id=";
	public static final String URL_RING_BY_CAT = SERVER_URL +"api.php?ringtone_cat_id=";
	public static final String URL_VIDEO_BY_CAT = SERVER_URL +"api.php?video_cat_id=";
	public static final String APP_DETAILS_URL = SERVER_URL +"api.php?app_details";
	public static final String URL_WALLPAPER_SINGLE = SERVER_URL +"api.php?wall_id=";
	public static final String URL_WALLPAPER_DOWNLOADS = SERVER_URL +"api.php?wall_download_id=";
	public static final String URL_RINGTONE_SINGLE = SERVER_URL +"api.php?ringtone_id=";
	public static final String URL_RINGTONE_DOWNLOADS = SERVER_URL +"api.php?ringtone_download_id=";
	public static final String URL_VIDEOS_SINGLE = SERVER_URL +"api.php?video_id=";
	public static final String URL_ABOUT_US_LOGO = SERVER_URL + "images/";
	public static final String URL_PUSH_NOTIFICATION = SERVER_URL + "api.php?android_token=";

	public static final String URL_UPLOAD_WALL = SERVER_URL + "api_wallpaper.php";
	public static final String URL_UPLOAD_WALL_RESP_1 = SERVER_URL + "api_wallpaper.php?cat_id=";
	public static final String URL_UPLOAD_WALL_RESP_2 = "&user_id=";
	public static final String URL_UPLOAD_WALL_RESP_3 = "&user_name=";
	public static final String URL_UPLOAD_WALL_RESP_4 = "&wallpaper_image_name=";
	public static final String URL_UPLOAD_WALL_RESP_5 = "&tags=";

	public static final String URL_GIF = SERVER_URL + "api.php?gif_id=";
	public static final String URL_GIFs_lIST = SERVER_URL + "api.php?gif_list";
	public static final String TAG_WALL_VIEWS="total_views";

	public static final String URL_UPLOAD_RING = SERVER_URL + "api_ringtone_upload.php";
	public static final String URL_UPLOAD_RING_THUMB = SERVER_URL + "api_ringtone_thumb.php";
	public static final String URL_UPLOAD_RING_RESP_1 = SERVER_URL + "api_ringtone.php?cat_id=";
	public static final String URL_UPLOAD_RING_RESP_2 = "&user_id=";
	public static final String URL_UPLOAD_RING_RESP_3 = "&user_name=";
	public static final String URL_UPLOAD_RING_RESP_4 = "&ringtone_type=";
	public static final String URL_UPLOAD_RING_RESP_5 = "&ringtone_title=";
	public static final String URL_UPLOAD_RING_RESP_6 = "&ringtone_thumbnail=";
	public static final String URL_UPLOAD_RING_RESP_7 = "&ringtone_duration=";
	public static final String URL_UPLOAD_RING_RESP_8 = "&tags=";
	public static final String URL_UPLOAD_RING_RESP_9 = "&ringtone_url=";

	public static final String URL_UPLOAD_VIDEO = SERVER_URL + "api_video_upload.php";
	public static final String URL_UPLOAD_VIDEO_THUMB = SERVER_URL + "api_video_thumb.php";
	public static final String URL_UPLOAD_VIDEO_RESP_1 = SERVER_URL + "api_video.php?cat_id=";
	public static final String URL_UPLOAD_VIDEO_RESP_2 = "&user_id=";
	public static final String URL_UPLOAD_VIDEO_RESP_3 = "&user_name=";
	public static final String URL_UPLOAD_VIDEO_RESP_4 = "&video_type=";
	public static final String URL_UPLOAD_VIDEO_RESP_5 = "&video_title=";
	public static final String URL_UPLOAD_VIDEO_RESP_6 = "&video_thumbnail=";
	public static final String URL_UPLOAD_VIDEO_RESP_7 = "&video_duration=";
	public static final String URL_UPLOAD_VIDEO_RESP_8 = "&tags=";
	public static final String URL_UPLOAD_VIDEO_RESP_9 = "&video_url=";
	public static final String URL_UPLOAD_VIDEO_RESP_10 = "&video_description=";

	public static final String URL_REGISTER_1 = SERVER_URL + "user_register_api.php?name=";
	public static final String URL_REGISTER_2 = "&email=";
	public static final String URL_REGISTER_3 = "&password=";
	public static final String URL_REGISTER_4 = "&phone=";

	public static final String URL_LOGIN_1 = SERVER_URL + "user_login_api.php?email=";
	public static final String URL_LOGIN_2 = "&password=";

	public static final String URL_FORGOT_PASS = SERVER_URL + "user_forgot_pass_api.php?email=";

	public static final String URL_PROFILE = SERVER_URL + "user_profile_api.php?id=";
	public static final String URL_PROFILE_EDIT_1 = SERVER_URL + "user_profile_update_api.php?user_id=";
	public static final String URL_PROFILE_EDIT_2 = "&name=";
	public static final String URL_PROFILE_EDIT_3 = "&email=";
	public static final String URL_PROFILE_EDIT_4 = "&password=";
	public static final String URL_PROFILE_EDIT_5 = "&phone=";
	public static final String URL_PROFILE_EDIT_6 = "&city=";
	public static final String URL_PROFILE_EDIT_7 = "&address=";

	public static final String URL_LATEST_WALLPAPER = SERVER_URL + "api.php?wall_latest";
	public static final String URL_LATEST_RINGTONE = SERVER_URL + "api.php?ringtone_latest";
	public static final String URL_LATEST_VIDEOS = SERVER_URL + "api.php?video_latest";
	public static final String URL_POPULAR_WALLPAPER = SERVER_URL + "api.php?wall_popular";
	public static final String URL_POPULAR_RINGTONE = SERVER_URL + "api.php?ringtone_popular";
	public static final String URL_POPULAR_VIDEOS = SERVER_URL + "api.php?video_popular";

	public static final String URL_RATING_1 = SERVER_URL + "api_rating.php?post_id=";
	public static final String URL_RATING_2 = "&post_type=";
	public static final String URL_RATING_3 = "&rate=";
	public static final String URL_RATING_4 = "&device_id=";

	public static final String URL_FEEDBACK_1 = SERVER_URL + "api.php?feedback&feed_name=";
	public static final String URL_FEEDBACK_2 = "&feed_email=";
	public static final String URL_FEEDBACK_3 = "&feed_msg=";

	public static final String TAG_ROOT="FUNDRIVE_APP";
	public static String TAG_WALL_LATEST = "wallpaper_latest";
	public static String TAG_VIDEO_LATEST = "video_latest";
	public static String TAG_RINGTONE_LATEST = "latest_ringtone";

	public static final String TAG_ID="id";

	public static final String TAG_CAT_ID="cid";
	public static final String TAG_CAT_NAME="category_name";
	public static final String TAG_CAT_IMAGE="category_image";
	public static final String TAG_CAT_IMAGE_THUMB="category_image_thumb";

	public static final String TAG_IMAGE_B="wallpaper_thumbnail_b";
	public static final String TAG_IMAGE_S="wallpaper_thumbnail_s";
	public static final String TAG_TAGS="tags";
	public static final String TAG_RATE_AVG="rate_avg";
	public static final String TAG_TOTAL_RATE="total_rate";
	public static final String TAG_TOTAL_VIEWS="total_views";
	public static final String TAG_TOTAL_DOWNLOADS="total_download";

	public static final String TAG_VIDEO_TYPE="video_type";
	public static final String TAG_VIDEO_TITLE="video_title";
	public static final String TAG_VIDEO_URL="video_url";
	public static final String TAG_VIDEO_ID="video_id";
	public static final String TAG_VIDEO_IMAGE_B="video_thumbnail_b";
	public static final String TAG_VIDEO_IMAGE_S="video_thumbnail_s";
	public static final String TAG_VIDEO_DESC="video_description";
	public static final String TAG_VIDEO_DURATION="video_duration";

	public static final String TAG_RINTONE_TYPE="ringtone_type";
	public static final String TAG_RINTONE_TITLE="ringtone_title";
	public static final String TAG_RINTONE_URL="ringtone_url";
	public static final String TAG_RINTONE_IMAGE_B="ringtone_thumbnail_b";
	public static final String TAG_RINTONE_IMAGE_S="ringtone_thumbnail_s";
	public static final String TAG_RINTONE_DURATION="ringtone_duration";

	public static final String TAG_MSG="msg";
	public static final String TAG_SUCCESS="success";

	public static final String TAG_NAME="name";

	public static final String TAG_USER_ID="user_id";
	public static final String TAG_USER_NAME="user_name";
	public static final String TAG_USER_EMAIL="email";
	public static final String TAG_USER_PHONE="phone";
	public static final String TAG_USER_CITY="city";
	public static final String TAG_USER_ADDRESS="address";


	public static final String TAG_WALL_ID="id";
	public static final String TAG_WALL_IMAGE="wallpaper_image";
	public static final String TAG_WALL_IMAGE_THUMB="wallpaper_image_thumb";
	public static final String TAG_GIF_IMAGE="gif_image";
	public static final String WALL_IMAGE_NAME = "wallpaper_image_name";

	// Number of columns of Grid View
	public static final int NUM_OF_COLUMNS = 2;
	public static final int NUM_OF_COLUMNS_NAV = 3;

	// Gridview image padding
	public static final int GRID_PADDING = 4; // in dp

	public static ArrayList<ItemWallpaper> arrayList_wall = new ArrayList<>();
	public static ArrayList<ItemRingtone> arrayList_ring = new ArrayList<>();
	public static ArrayList<ItemVideos> arrayList_video = new ArrayList<>();
	public static ArrayList<ItemGIF> arrayList_gif = new ArrayList<>();

	public static UserItem userItem;
	public static String user_id = "";

	public static Boolean isUpdate = false;
	public static Boolean isLogged = false;

	public static Boolean isLoginChanged = false;

	public static ItemAbout itemAbout;

	public static int adCount = 0;
	public static int adShow = 3;

	public static int dataLoadNumber = 10;

	public static final String DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE="/glitter/Ringtone/";
	public static final String DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER="/glitter/Wallpaper/";
	public static final String DOWNLOAD_SDCARD_FOLDER_PATH_VIDEOS="/glitter/Videos";
}
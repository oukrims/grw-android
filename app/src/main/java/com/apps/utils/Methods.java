package com.apps.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.apps.RW.LoginActivity;
import com.apps.RW.R;
import com.apps.items.UserItem;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class Methods {

    private Context _context;
    private SharedPreferences sharedPreferences;
    private  SharedPreferences.Editor editor;
    InterstitialAd mInterstitial;

    public Methods(Context context){
        this._context = context;
        sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfoMob = cm.getNetworkInfo(cm.TYPE_MOBILE);
        NetworkInfo netInfoWifi = cm.getNetworkInfo(cm.TYPE_WIFI);
        if ((netInfoMob != null && netInfoMob.isConnectedOrConnecting()) || (netInfoWifi != null && netInfoWifi.isConnectedOrConnecting())) {
            return true;
        }
        return false;
    }

    public int getScreenWidth() {
        int columnWidth;
        WindowManager wm = (WindowManager) _context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();

        point.x = display.getWidth();
        point.y = display.getHeight();

        columnWidth = point.x;
        return columnWidth;
    }

    public int getScreenHeight() {
        int height;
        WindowManager wm = (WindowManager) _context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();

        point.x = display.getWidth();
        point.y = display.getHeight();

        height = point.y;
        return height;
    }

    private static void openLogin(Context context) {
        Intent intent = new Intent(context,LoginActivity.class);
        intent.putExtra("from","app");
        context.startActivity(intent);
    }

//    public static void setMenu(Menu menu, Context context) {
//        if(Constant.isLogged) {
//            menu.findItem(R.id.menu_login).setTitle(context.getResources().getString(R.string.logout));
//            menu.findItem(R.id.menu_login).setIcon(context.getResources().getDrawable(R.drawable.logout));
//        } else {
//            menu.findItem(R.id.menu_login).setTitle(context.getResources().getString(R.string.login));
//            menu.findItem(R.id.menu_login).setIcon(context.getResources().getDrawable(R.drawable.login));
//        }
//    }

//    public void setLoginButton(Button button, Context context) {
//        if(Constant.isLogged) {
//            button.setText(context.getResources().getString(R.string.logout));
//            button.setBackground(context.getResources().getDrawable(R.drawable.bg_btn_logout));
//        } else {
//            button.setText(context.getResources().getString(R.string.login));
//            button.setBackground(context.getResources().getDrawable(R.drawable.bg_btn_login));
//        }
//    }

    private void logout(Activity activity) {

        changeRemPass(false);
        Constant.userItem = new UserItem("","","","","","");
        Constant.user_id = "";
        editor.putString("uid","");
        editor.putString("user","");
        editor.putString("pass","");
        editor.putString("fbtoken","");
        editor.apply();
        Constant.isLogged = false;
        Intent intent1 = new Intent(_context,LoginActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(intent1);
        activity.finish();
    }

    public void clickLogin() {
        if(Constant.isLogged) {
            logout((Activity)_context);
            ((Activity) _context).finish();
            Toast.makeText(_context, _context.getResources().getString(R.string.logout_success), Toast.LENGTH_SHORT).show();
        } else {
//                    openLoginDialog();
//			Intent intent = new Intent(_context,LoginActivity.class);
//			intent.putExtra("from","app");
//			_context.startActivity(intent);
            openLogin(_context);
        }
    }

    public void setStatusColor(Window window, Toolbar toolbar)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(_context.getResources().getColor(R.color.status_bar));
            if(toolbar != null) {
                toolbar.setElevation(10);
            }
        }
    }

    public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            return packageManager.getApplicationInfo(packagename, 0).enabled;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

//    public void startVideoPlay(Activity activity,String vid) {
//        if(isPackageInstalled("com.google.android.youtube",_context.getPackageManager())) {
//            Intent intent = YouTubeStandalonePlayer.createVideoIntent(activity, _context.getResources().getString(R.string.youtube_api_key), vid, 0, true, false);
//            _context.startActivity(intent);
//        } else {
//            Toast.makeText(_context, _context.getResources().getString(R.string.install_yt), Toast.LENGTH_SHORT).show();
//        }
//    }

//    public void setFavImage(Boolean isFav, ImageView imageView) {
//        if(isFav) {
//            imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.fav_hover_pink));
//        } else {
//            imageView.setImageDrawable(_context.getResources().getDrawable(R.drawable.fav));
//        }
//    }

//    public void shareNews(ItemNews News) {
//        itemNews = News;
//        saveImage(Constant.URL_IMAGE + itemNews.getImage(),itemNews.getImage());
////		Intent intent = new Intent(Intent.ACTION_SEND);
////		intent.setType("image/*");
////		intent.setData(Uri.parse("file://" + filepath));
////		intent.putExtra(Intent.EXTRA_TEXT,itemNews.getHeading() + "/n/n" + itemNews.getDesc());
////		_context.startActivity(intent);
//    }
//
//    public void saveImage(String img_url, String name) {
//        new LoadShare().execute(img_url,name);
//    }
//
//    public class LoadShare extends AsyncTask<String, String, String> {
//
//        @Override
//        protected String doInBackground(String... strings) {
//            String filepath="";
//            String name = strings[1];
//            try
//            {
//                File SDCardRoot = _context.getExternalCacheDir().getAbsoluteFile();
//                File file = new File(SDCardRoot,name);
//                if(!file.exists()) {
//                    URL url = new URL(strings[0]);
//                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                    urlConnection.setRequestMethod("GET");
//                    urlConnection.setDoOutput(true);
//                    urlConnection.connect();
//
//                    Log.i("Local filename:", "" + name);
//                    if (file.createNewFile()) {
//                        file.createNewFile();
//                    }
//                    FileOutputStream fileOutput = new FileOutputStream(file);
//                    InputStream inputStream = urlConnection.getInputStream();
//                    int totalSize = urlConnection.getContentLength();
//                    int downloadedSize = 0;
//                    byte[] buffer = new byte[1024];
//                    int bufferLength = 0;
//                    while ((bufferLength = inputStream.read(buffer)) > 0) {
//                        fileOutput.write(buffer, 0, bufferLength);
//                        downloadedSize += bufferLength;
//                        Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
//                    }
//                    fileOutput.close();
//                    if (downloadedSize == totalSize) {
//                        filepath = file.getPath();
//                    }
//                } else {
//                    filepath = file.getPath();
//                }
//            }
//            catch (MalformedURLException e)
//            {
//                e.printStackTrace();
//            }
//            catch (IOException e)
//            {
//                filepath=null;
//                e.printStackTrace();
//            }
//            Log.i("filepath:"," "+filepath) ;
//            return filepath;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//
//            Spanned spanned = Html.fromHtml(itemNews.getDesc());
//            char[] chars = new char[spanned.length()];
//            TextUtils.getChars(spanned, 0, spanned.length(), chars, 0);
//            String plainText = new String(chars);
//
//            Intent intent = new Intent(Intent.ACTION_SEND);
//            intent.setType("image/*");
//
//            if(!itemNews.getVideoUrl().trim().isEmpty()) {
//                intent.putExtra(Intent.EXTRA_TEXT, itemNews.getHeading() +"\n\n"+ itemNews.getVideoUrl() + "\n\n" + plainText);
//            } else {
//                intent.putExtra(Intent.EXTRA_TEXT, itemNews.getHeading() + "\n\n" + plainText);
//            }
//            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + s));
//            _context.startActivity(Intent.createChooser(intent, _context.getResources().getString(R.string.share_news)));
//            super.onPostExecute(s);
//        }
//    }

    public void changeRemPass(Boolean rem) {
        editor.putBoolean("rem",rem);
        editor.commit();
    }

//    public void setProfileVar(EditText editText_fname, EditText editText_lname, EditText editText_email, EditText editText_address, EditText editText_city, EditText editText_phone, EditText editText_postal) {
//        editText_fname.setText(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getFName());
//        editText_lname.setText(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getLname());
//
//        editText_address.setText(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getAddress());
//        editText_city.setText(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getCity());
//        editText_phone.setText(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getMobile());
//        editText_postal.setText(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getPostalCode());
//
//        if(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getEmail().contains("fdnad")) {
//            editText_email.setText("");
//        } else {
//            editText_email.setText(Constants.arrayList_user.get(Constants.arrayList_user.size()-1).getEmail());
//        }
//    }

//    public void setMenu(Menu menu) {
//        if(Constants.isLogged) {
//            menu.findItem(R.id.item_logout).setTitle(_context.getResources().getString(R.string.logout));
//            menu.findItem(R.id.item_profile).setVisible(true);
//            menu.findItem(R.id.item_cart).setVisible(true);
//            menu.findItem(R.id.item_orderlist).setVisible(true);
//        } else {
//            menu.findItem(R.id.item_logout).setTitle(_context.getResources().getString(R.string.login));
//            menu.findItem(R.id.item_profile).setVisible(false);
//            menu.findItem(R.id.item_cart).setVisible(false);
//            menu.findItem(R.id.item_orderlist).setVisible(false);
//        }
//    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported(Window window)
    {
        if(_context.getResources().getString(R.string.isRTL).equals("true")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                window.getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
        }
    }

    public void shareVideo(int pos, String image, String title, String video_url) {
        (new ShareTask(_context)).execute(image, String.valueOf(pos),title, video_url);
    }

    private class ShareTask extends AsyncTask<String , String , String>
    {
        int pos;
        String title, video_url;
        private Context context;
        private ProgressDialog pDialog;
        URL myFileUrl;
        Bitmap bmImg = null;
        File file ;

        private ShareTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

            pDialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(context.getResources().getString(R.string.please_wait));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub
            pos = Integer.parseInt(args[1]);
            title = args[2];
            video_url = args[3];
            try {

                myFileUrl = new URL(args[0]);
                //myFileUrl1 = args[0];

                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bmImg = BitmapFactory.decodeStream(is);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            try {

                String path = myFileUrl.getPath();
                String idStr = path.substring(path.lastIndexOf('/') + 1);
                File filepath = context.getExternalCacheDir();
                File dir = null;
                if (filepath != null) {
                    dir = new File(filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_VIDEOS);
                }
                dir.mkdirs();
                String fileName = idStr;
                file = new File(dir, fileName);
                FileOutputStream fos = new FileOutputStream(file);
                bmImg.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String args) {
            // TODO Auto-generated method stub

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            share.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.title) + " - " + title + "\n\n" + video_url);
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getAbsolutePath()));
            context.startActivity(Intent.createChooser(share, context.getResources().getString(R.string.share_video)));
            pDialog.dismiss();
        }
    }

    public static void setFavImage(Boolean fav, ImageView image) {
        if(fav) {
            image.setImageResource(R.mipmap.heart_red_round_hover);
        } else {
            image.setImageResource(R.mipmap.heart_red_round);
        }
    }

    public static void setFavImageRing(Boolean fav, ImageView image) {
        if(fav) {
            image.setImageResource(R.drawable.fav_rect_hover);
        } else {
            image.setImageResource(R.drawable.fav_rect);
        }
    }

    public String getPathImage(Uri uri) {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                String filePath = "";
                String wholeID = DocumentsContract.getDocumentId(uri);

                // Split at colon, use second item in the array
                String id = wholeID.split(":")[1];

                String[] column = {MediaStore.Images.Media.DATA};

                // where id is equal to
                String sel = MediaStore.Images.Media._ID + "=?";

                Cursor cursor = _context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);

                int columnIndex = cursor.getColumnIndex(column[0]);

                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
                return filePath;
            } else {

                if (uri == null) {
                    return null;
                }
                // try to retrieve the image from the media store first
                // this will only work for images selected from gallery
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = _context.getContentResolver().query(uri, projection, null, null, null);
                if (cursor != null) {
                    int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    String retunn = cursor.getString(column_index);
                    cursor.close();
                    return retunn;
                }
                // this is our fallback here
                return uri.getPath();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (uri == null) {
                return null;
            }
            // try to retrieve the image from the media store first
            // this will only work for images selected from gallery
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = _context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String returnn = cursor.getString(column_index);
                cursor.close();
                return returnn;
            }
            // this is our fallback here
            return uri.getPath();
        }
    }
}

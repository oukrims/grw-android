package com.apps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.RW.R;
import com.apps.items.ItemRingtone;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterRingtone extends RecyclerView.Adapter<AdapterRingtone.MyViewHolder> {

    private Context context;
    private ArrayList<ItemRingtone> arrayList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView_title;
        RoundedImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            textView_title = (TextView)view.findViewById(R.id.tv_home_ringtone);
            imageView = (RoundedImageView) view.findViewById(R.id.iv_latest_ringtone);
        }
    }

    public AdapterRingtone(Context context, ArrayList<ItemRingtone> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_ringtone, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.textView_title.setText(arrayList.get(position).getTitle());
        Picasso.with(context)
                .load(arrayList.get(position).getImageSmall())
                .into(holder.imageView);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
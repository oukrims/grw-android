package com.apps.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.RW.R;
import com.apps.items.ItemVideos;
import com.apps.utils.Methods;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterVideoPager  extends PagerAdapter {

    private Methods methods;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<ItemVideos> arrayList;

    public AdapterVideoPager(Context context, ArrayList<ItemVideos> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        methods = new Methods(context);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View imageLayout = inflater.inflate(R.layout.viewpager_home_video, container, false);
        assert imageLayout != null;

        RoundedImageView imageView = (RoundedImageView) imageLayout.findViewById(R.id.iv_home_video);
        TextView textView_title = (TextView) imageLayout.findViewById(R.id.tv_home_video_title);
        TextView textView_views = (TextView) imageLayout.findViewById(R.id.tv_home_views_videos);
        LinearLayout ll_share = (LinearLayout)imageLayout.findViewById(R.id.ll_home_video_share);

        textView_title.setText(arrayList.get(position).getTitle());
        textView_views.setText(arrayList.get(position).getTotalViews());

        if(!arrayList.get(position).getImageBig().trim().isEmpty()) {
            Picasso.with(context)
                    .load(arrayList.get(position).getImageBig())
                    .placeholder(R.mipmap.app_icon)
                    .into(imageView);
        } else {
            Picasso.with(context)
                    .load(R.mipmap.app_icon)
                    .into(imageView);
        }

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                methods.shareVideo(position,arrayList.get(position).getImageBig(),arrayList.get(position).getTitle(),arrayList.get(position).getUrl());
            }
        });

        container.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}

package com.apps.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.RW.DetailWallpaper;
import com.apps.RW.R;
import com.apps.items.ItemWallpaper;
import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.Methods;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;
import com.bumptech.glide.GlideBuilder;


import java.util.ArrayList;


public class AdapterLatestWallpaper extends RecyclerView.Adapter<AdapterLatestWallpaper.MyViewHolder> {

    private Context context;
    private ArrayList<ItemWallpaper> arrayList;
    private ArrayList<ItemWallpaper> filteredArrayList;
    private NameFilter filter;
    private Methods methods;
    int columnWidth;
    private DBHelper dbHelper;
    private Boolean isFav;
    public static  int  Clicks;
    public static int media;
    InterstitialAd mInterstitial;

    public AdapterLatestWallpaper(Context context, ArrayList<ItemWallpaper> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        methods = new Methods(context);
        this.filteredArrayList = arrayList;
        dbHelper = new DBHelper(context);

        loadInterAd();

        Resources r = context.getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Constant.GRID_PADDING, r.getDisplayMetrics());
        columnWidth = (int) ((methods.getScreenWidth() - ((Constant.NUM_OF_COLUMNS + 1) * padding)) / Constant.NUM_OF_COLUMNS);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView_view, textView_downloads;
        RoundedImageView imageView;
        ImageView imageView_fav;
        View views;


        public MyViewHolder(View view) {
            super(view);
            textView_view = (TextView)view.findViewById(R.id.tv_views_latest_wall);
            textView_downloads = (TextView)view.findViewById(R.id.tv_downloads_latest_wall);
            imageView = (RoundedImageView) view.findViewById(R.id.iv_latest_wall);
            imageView_fav = (ImageView) view.findViewById(R.id.iv_fav_latest_wall);
            views = (View)view.findViewById(R.id.view_latest_wall);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_latest_wallpaper, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        isFav = dbHelper.isFav(arrayList.get(position).getId(),"wall");
        Methods.setFavImage(isFav, holder.imageView_fav);

        ((MyViewHolder) holder).imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        ((MyViewHolder) holder).imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth));

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(columnWidth, columnWidth/2);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        ((MyViewHolder) holder).views.setLayoutParams(params);


        holder.textView_view.setText(arrayList.get(position).getTotalViews());
        holder.textView_downloads.setText(arrayList.get(position).getTotalDownload());
        /*Picasso.with(context)
                .load(arrayList.get(position).getImageSmall())
                .placeholder(R.drawable.placeholder_small)
                .into(holder.imageView);*/
        if(arrayList.get(position).getImageBig().endsWith("gif")){
            Glide.with(context)
                    .load(arrayList.get(position).getImageBig())
                    .asGif()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.placeholder_small)
                    .into(holder.imageView);
            media = 0;

        }else {
            Glide.with(context)
                    .load(arrayList.get(position).getImageBig())
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.placeholder_small)
                    .into(holder.imageView);
            media = 1;
        }

        holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFav = dbHelper.isFav(arrayList.get(holder.getAdapterPosition()).getId(),"wall");
                if(!isFav) {
                    dbHelper.addFavWall(arrayList.get(holder.getAdapterPosition()));
                    isFav = true;
                    Toast.makeText(context, context.getResources().getString(R.string.added_fav), Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.removeFav(arrayList.get(holder.getAdapterPosition()).getId(),"wallpaper");
                    isFav = false;
                    Toast.makeText(context, context.getResources().getString(R.string.removed_fav), Toast.LENGTH_SHORT).show();
                }
                Methods.setFavImage(isFav, holder.imageView_fav);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.arrayList_wall.clear();
                Constant.arrayList_wall.addAll(arrayList);
                Clicks++;
                if(Clicks > 2){
                    showInterAd();
                    Clicks = 0;
                    Log.d("Now","clicks "+Clicks);
                }
                Intent intent = new Intent(context, DetailWallpaper.class);
                int realpos = getPosition(getID(holder.getAdapterPosition()));
                intent.putExtra("pos", realpos);
                intent.putExtra("media", media);

                    context.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    public String getID(int pos) {
        return arrayList.get(pos).getId();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public Filter getFilter() {
        if (filter == null){
            filter  = new NameFilter();
        }
        return filter;
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                ArrayList<ItemWallpaper> filteredItems = new ArrayList<>();

                for (int i = 0, l = filteredArrayList.size(); i < l; i++) {
                    String nameList = filteredArrayList.get(i).getTags();
                    if (nameList.toLowerCase().contains(constraint))
                        filteredItems.add(filteredArrayList.get(i));
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = filteredArrayList;
                    result.count = filteredArrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            arrayList = (ArrayList<ItemWallpaper>) results.values;
            notifyDataSetChanged();
        }
    }

    public void showInterAd() {

        if (mInterstitial.isLoaded()) {
            mInterstitial.show();
            mInterstitial.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    loadInterAd();
                    super.onAdClosed();
                }
            });
        } else {
            Log.d("InterAd", "The interstitial wasn't loaded yet.");
        }
    }

    public void loadInterAd() {
        mInterstitial = new InterstitialAd(context);
        mInterstitial.setAdUnitId(context.getResources().getString(R.string.admob_intertestial_id));
        mInterstitial.loadAd(new AdRequest.Builder().build());
    }

    private int getPosition(String id) {
        int count = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (id.equals(arrayList.get(i).getId())) {
                count = i;
                break;
            }
        }
        return count;
    }

//    public boolean isHeader(int position) {
//        return position == arrayList.size();
//    }
}
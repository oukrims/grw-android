package com.apps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.RW.DetailWallpaper;
import com.apps.RW.R;
import com.apps.items.ItemWallpaper;
import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.Methods;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterWallpaper extends RecyclerView.Adapter<AdapterWallpaper.MyViewHolder> {

    private Context context;
    private ArrayList<ItemWallpaper> arrayList;
    private DBHelper dbHelper;
    private Boolean isFav;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView_view, textView_downloads;
        RoundedImageView imageView;
        ImageView imageView_fav;

        public MyViewHolder(View view) {
            super(view);
            textView_view = (TextView)view.findViewById(R.id.tv_views_wall_list);
            textView_downloads = (TextView)view.findViewById(R.id.tv_downloads_wall_list);
            imageView = (RoundedImageView) view.findViewById(R.id.iv_home_wall);
            imageView_fav = (ImageView) view.findViewById(R.id.iv_fav_wall_list);
        }
    }

    public AdapterWallpaper(Context context, ArrayList<ItemWallpaper> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        dbHelper = new DBHelper(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_wallpaper, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        isFav = dbHelper.isFav(arrayList.get(position).getId(),"wall");
        Methods.setFavImage(isFav, holder.imageView_fav);

        holder.textView_view.setText(arrayList.get(position).getTotalViews());
        holder.textView_downloads.setText(arrayList.get(position).getTotalDownload());
        Picasso.with(context)
                .load(arrayList.get(position).getImageSmall())
                .into(holder.imageView);

        holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFav = dbHelper.isFav(arrayList.get(holder.getAdapterPosition()).getId(),"wall");
                if(!isFav) {
                    dbHelper.addFavWall(arrayList.get(holder.getAdapterPosition()));
                    isFav = true;
                    Toast.makeText(context, context.getResources().getString(R.string.added_fav), Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.removeFav(arrayList.get(holder.getAdapterPosition()).getId(),"wallpaper");
                    isFav = false;
                    Toast.makeText(context, context.getResources().getString(R.string.removed_fav), Toast.LENGTH_SHORT).show();
                }
                Methods.setFavImage(isFav, holder.imageView_fav);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.arrayList_wall.clear();
                Constant.arrayList_wall.addAll(arrayList);
                Intent intent = new Intent(context,DetailWallpaper.class);
                intent.putExtra("pos",holder.getAdapterPosition());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
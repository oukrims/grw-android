package com.apps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.RW.DetailRingtone;
import com.apps.RW.R;
import com.apps.items.ItemRingtone;
import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.Methods;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterLatestRingtone extends RecyclerView.Adapter<AdapterLatestRingtone.MyViewHolder> {

    private Context context;
    private ArrayList<ItemRingtone> arrayList;
    private ArrayList<ItemRingtone> filteredArrayList;
    private NameFilter filter;
    private DBHelper dbHelper;
    private Boolean isFav;

    public AdapterLatestRingtone(Context context, ArrayList<ItemRingtone> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        dbHelper = new DBHelper(context);
        this.filteredArrayList = arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView_title, textView_cat, textView_downlaods;
        RoundedImageView imageView;
        ImageView imageView_fav;
        AppCompatRatingBar ratingBar;
        LinearLayout ll;

        public MyViewHolder(View view) {
            super(view);
            ll = (LinearLayout) view.findViewById(R.id.ll_latest_ring);
            textView_title = (TextView)view.findViewById(R.id.tv_latest_ring_title);
            textView_cat = (TextView)view.findViewById(R.id.tv_latest_ring_cat);
            textView_downlaods = (TextView)view.findViewById(R.id.tv_latest_ring_downloads);
            imageView = (RoundedImageView) view.findViewById(R.id.iv_latset_ringtone);
            imageView_fav = (ImageView) view.findViewById(R.id.iv_fav_latest_ring);
          //  ratingBar = (AppCompatRatingBar)view.findViewById(R.id.rating_latest_ringtone);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_latest_ringtone, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

       if(!dbHelper.isFav(arrayList.get(position).getId(),"ring")) {
           holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_rect));
      } else {
          holder.imageView_fav.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_rect_hover));
     }

        isFav = dbHelper.isFav(arrayList.get(position).getId(),"ring");
        Methods.setFavImageRing(isFav, holder.imageView_fav);

        holder.textView_title.setText(arrayList.get(position).getTitle());
        holder.textView_cat.setText("Ringtones");
        holder.textView_downlaods.setText(arrayList.get(position).getTotalDownload());
        Picasso.with(context)
                .load(arrayList.get(position).getImageSmall())
                .placeholder(R.drawable.placeholder_small)
                .into(holder.imageView);
        Log.d("rate","rating avg : "+arrayList.get(position).getRateAvg());

        if(arrayList.get(position).getRateAvg().trim().isEmpty()) {
            holder.ratingBar.setRating(0);
        } else {
         //  holder.ratingBar.setRating(Float.parseFloat(arrayList.get(position).getRateAvg()));
        }

        holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFav = dbHelper.isFav(arrayList.get(holder.getAdapterPosition()).getId(),"ring");
                if(!isFav) {
                    dbHelper.addFavRing(arrayList.get(holder.getAdapterPosition()));
                    isFav = true;
                    Toast.makeText(context, context.getResources().getString(R.string.added_fav), Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.removeFav(arrayList.get(holder.getAdapterPosition()).getId(),"ringtone");
                    isFav = false;
                    Toast.makeText(context, context.getResources().getString(R.string.removed_fav), Toast.LENGTH_SHORT).show();
                }
                Methods.setFavImageRing(isFav, holder.imageView_fav);
            }
        });

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.arrayList_ring.clear();
                Constant.arrayList_ring.addAll(arrayList);
                Intent intent = new Intent(context,DetailRingtone.class);
                int realpos = getPosition(getID(holder.getAdapterPosition()));
                intent.putExtra("pos",realpos);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public String getID(int pos) {
        return arrayList.get(pos).getId();
    }

    public Filter getFilter() {
        if (filter == null){
            filter  = new NameFilter();
        }
        return filter;
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                ArrayList<ItemRingtone> filteredItems = new ArrayList<>();

                for (int i = 0, l = filteredArrayList.size(); i < l; i++) {
                    String tagList = filteredArrayList.get(i).getTags();
                    String nameList = filteredArrayList.get(i).getTitle();
                    if (nameList.toLowerCase().contains(constraint) || tagList.toLowerCase().contains(constraint))
                        filteredItems.add(filteredArrayList.get(i));
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = filteredArrayList;
                    result.count = filteredArrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            arrayList = (ArrayList<ItemRingtone>) results.values;
            notifyDataSetChanged();
        }
    }

    private int getPosition(String id) {
        int count = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (id.equals(arrayList.get(i).getId())) {
                count = i;
                break;
            }
        }
        return count;
    }
}
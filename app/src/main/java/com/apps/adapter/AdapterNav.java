package com.apps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.RW.R;
import com.apps.utils.Constant;
import com.apps.utils.Methods;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterNav extends RecyclerView.Adapter {

    private Context context;
    ArrayList<String> title;
    ArrayList<Integer> image;
    int columnWidth;
    Methods methods;

    private class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView,imageView_icon;
        private TextView textView_title;

        private MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.item);
            imageView_icon = (ImageView) view.findViewById(R.id.iv_nav_icon);
            textView_title = (TextView) view.findViewById(R.id.tv_nav_title);
        }
    }

    public AdapterNav(Context context) {
        this.context = context;
        methods = new Methods(context);

       /* Resources r = context.getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Constant.GRID_PADDING, r.getDisplayMetrics());
        columnWidth = (int) ((methods.getScreenWidth() - ((Constant.NUM_OF_COLUMNS_NAV + 1) * padding)) / Constant.NUM_OF_COLUMNS_NAV);
*/
        image = new ArrayList<>();
        image.add(R.mipmap.home_nav);
        image.add(R.mipmap.wallpaper);
        image.add(R.mipmap.ringtone);
        image.add(R.mipmap.videos);
        image.add(R.mipmap.favnav);
        image.add(R.mipmap.upload);
        image.add(R.mipmap.profile);
        image.add(R.mipmap.feedback);
        image.add(R.mipmap.about);

        image.add(R.mipmap.more);
        image.add(R.mipmap.share);
        image.add(R.mipmap.rate);
        image.add(R.mipmap.privacy);

        if(!Constant.isLogged) {
            image.add(R.mipmap.login);
        } else {
            image.add(R.mipmap.logout);
        }

        title = new ArrayList<>();
        title.add(context.getResources().getString(R.string.home));
        title.add(context.getResources().getString(R.string.wallpaper));
        title.add(context.getResources().getString(R.string.ringtone));
        title.add(context.getResources().getString(R.string.gifs));
        title.add(context.getResources().getString(R.string.fav));
        title.add(context.getResources().getString(R.string.upload));
        title.add(context.getResources().getString(R.string.profile));
        title.add(context.getResources().getString(R.string.feedback));
        title.add(context.getResources().getString(R.string.about));
        title.add(context.getResources().getString(R.string.moreapp));
        title.add(context.getResources().getString(R.string.shareapp));
        title.add(context.getResources().getString(R.string.rateapp));
        title.add(context.getResources().getString(R.string.privacy));
        if(!Constant.isLogged) {
            title.add(context.getResources().getString(R.string.log_in));
        } else {
            title.add(context.getResources().getString(R.string.log_out));
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_nav, parent, false);

            return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if(position % 2 == 0) {
            ((MyViewHolder) holder).imageView.setBackgroundDrawable(context.getResources().getDrawable(R.color.nav_item_bg_30));
        } else {
            ((MyViewHolder) holder).imageView.setBackgroundDrawable(context.getResources().getDrawable(R.color.nav_item_bg_80));
        }

        ((MyViewHolder) holder).imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        ((MyViewHolder) holder).imageView.setLayoutParams(new RelativeLayout.LayoutParams(columnWidth, columnWidth));
        ((MyViewHolder) holder).textView_title.setText(title.get(position));

        Picasso.with(context)
                .load(image.get(position))
                .placeholder(R.mipmap.ic_launcher)
                .into(((MyViewHolder) holder).imageView_icon);
    }

    @Override
    public int getItemCount() {
        return image.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
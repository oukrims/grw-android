package com.apps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.RW.DetailVideos;
import com.apps.RW.R;
import com.apps.items.ItemVideos;
import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.Methods;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterLatestVideo extends RecyclerView.Adapter<AdapterLatestVideo.MyViewHolder> {

    private Context context;
    private ArrayList<ItemVideos> arrayList;
    private Methods methods;
    private ArrayList<ItemVideos> filteredArrayList;
    private NameFilter filter;
    private DBHelper dbHelper;
    private Boolean isFav;

    public AdapterLatestVideo(Context context, ArrayList<ItemVideos> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        methods = new Methods(context);
        this.filteredArrayList = arrayList;
        dbHelper = new DBHelper(context);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView_title,textView_views;
        RoundedImageView imageView;
        ImageView imageView_fav;
        View views;
        LinearLayout ll_share;

        public MyViewHolder(View view) {
            super(view);
            textView_title = (TextView)view.findViewById(R.id.tv_title_latest_video);
            imageView = (RoundedImageView) view.findViewById(R.id.iv_latest_video);
            imageView_fav = (ImageView) view.findViewById(R.id.iv_fav_latest_videos);
            views = (View)view.findViewById(R.id.view_latest_video);
            textView_views = (TextView) view.findViewById(R.id.tv_latest_views_videos);
            ll_share = (LinearLayout)view.findViewById(R.id.ll_latest_video_share);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_latest_videos, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        isFav = dbHelper.isFav(arrayList.get(position).getId(),"video");
        Methods.setFavImageRing(isFav, holder.imageView_fav);

        ((MyViewHolder) holder).imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        holder.textView_views.setText(arrayList.get(position).getTotalViews());
        holder.textView_title.setText(arrayList.get(position).getTitle());
        Picasso.with(context)
                .load(arrayList.get(position).getImageBig())
                .placeholder(R.drawable.placeholder_small)
                .into(holder.imageView);

        holder.imageView_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFav = dbHelper.isFav(arrayList.get(holder.getAdapterPosition()).getId(),"video");
                if(!isFav) {
                    dbHelper.addFavVideo(arrayList.get(holder.getAdapterPosition()));
                    isFav = true;
                    Toast.makeText(context, context.getResources().getString(R.string.added_fav), Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.removeFav(arrayList.get(holder.getAdapterPosition()).getId(),"video");
                    isFav = false;
                    Toast.makeText(context, context.getResources().getString(R.string.removed_fav), Toast.LENGTH_SHORT).show();
                }
                Methods.setFavImageRing(isFav, holder.imageView_fav);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constant.arrayList_video.clear();
                Constant.arrayList_video.addAll(arrayList);
                Intent intent = new Intent(context,DetailVideos.class);
                int realpos = getPosition(getID(holder.getAdapterPosition()));
                intent.putExtra("pos",realpos);
                context.startActivity(intent);
            }
        });

        holder.ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                methods.shareVideo(holder.getAdapterPosition(),arrayList.get(holder.getAdapterPosition()).getImageBig(),arrayList.get(holder.getAdapterPosition()).getTitle(),arrayList.get(holder.getAdapterPosition()).getUrl());
            }
        });
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public String getID(int pos) {
        return arrayList.get(pos).getId();
    }

    public Filter getFilter() {
        if (filter == null){
            filter  = new NameFilter();
        }
        return filter;
    }

    private class NameFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                ArrayList<ItemVideos> filteredItems = new ArrayList<>();

                for (int i = 0, l = filteredArrayList.size(); i < l; i++) {
                    String tagList = filteredArrayList.get(i).getTags();
                    String nameList = filteredArrayList.get(i).getTitle();
                    if (tagList.toLowerCase().contains(constraint) || nameList.toLowerCase().contains(constraint))
                        filteredItems.add(filteredArrayList.get(i));
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = filteredArrayList;
                    result.count = filteredArrayList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            arrayList = (ArrayList<ItemVideos>) results.values;
            notifyDataSetChanged();
        }
    }

    private int getPosition(String id) {
        int count = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (id.equals(arrayList.get(i).getId())) {
                count = i;
                break;
            }
        }
        return count;
    }
}

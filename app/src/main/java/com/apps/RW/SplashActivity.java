package com.apps.RW;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apps.items.UserItem;
import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    Boolean isRemember = false;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Methods methods;
    String username = "", password = "";
    ProgressBar progressbar_login;
    Boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        methods = new Methods(SplashActivity.this);
        methods.setStatusColor(getWindow(),null);
        hideStatusBar();

        progressbar_login = (ProgressBar) findViewById(R.id.progressbar_login);
        sharedPreferences = getSharedPreferences("login", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        isFirst = sharedPreferences.getBoolean("appfirst",true);
        isRemember = sharedPreferences.getBoolean("rem", false);
        username = sharedPreferences.getString("user", "");
        password = sharedPreferences.getString("pass", "");
        Constant.user_id = sharedPreferences.getString("uid", "");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isRemember) {
                    if (methods.isNetworkAvailable()) {
                        Log.e("username::", username);
                        Log.e("password::", password);
                        new LoadLogin().execute(username, password);
                    } else {
                        Toast.makeText(SplashActivity.this, getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent;
                    if(isFirst) {
                        intent = new Intent(SplashActivity.this, IntroActivity.class);
                    } else {
                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                    }
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);
    }

    class LoadLogin extends AsyncTask<String, String, String> {

        String suc = "", msg = "";

        @Override
        protected void onPreExecute() {
            progressbar_login.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String user = args[0];
            String pass = args[1];

            String json_strng = JsonUtils.getJSONString(Constant.URL_LOGIN_1 + user + Constant.URL_LOGIN_2 + pass);
            try {
                JSONObject jsonObject = new JSONObject(json_strng);
                JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG_ROOT);

                JSONObject object = jsonArray.getJSONObject(0);
                suc = object.getString(Constant.TAG_SUCCESS);
                if (suc.equals("0")) {
                    msg = object.getString(Constant.TAG_MSG);
                    return "0";
                } else {
                    Constant.user_id = object.getString(Constant.TAG_USER_ID);
                    Constant.userItem = new UserItem(Constant.user_id, object.getString(Constant.TAG_NAME), "", "", "", "");
                    return "1";
                }

            } catch (JSONException e) {
                e.printStackTrace();
                return "0";
            } catch (Exception e) {
                e.printStackTrace();
                return "0";
            }
        }

        protected void onPostExecute(String success) {
            progressbar_login.setVisibility(View.GONE);
            if (success.equals("1")) {
//                if(isRemember) {
//                    editor.putBoolean("rem", true);
//                    editor.putString("user",username);
//                    editor.putString("pass",password);
//                    editor.putString("uid",Constants.user_id);
//                    editor.commit();
//                }
                Constant.isLogged = true;
                Toast.makeText(SplashActivity.this, getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else if (success.equals("0")) {
                Toast.makeText(SplashActivity.this, getResources().getString(R.string.login_not_success), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }

    }

    void hideStatusBar() {
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }
}
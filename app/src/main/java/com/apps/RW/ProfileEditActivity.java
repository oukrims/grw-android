package com.apps.RW;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfileEditActivity extends AppCompatActivity {

    private EditText editText_name,editText_email,editText_phone,editText_pass,editText_cpass,editText_city,editText_address;
    private AppCompatButton button_update;
    private String name,email,phone,password="",address,city;
    Toolbar toolbar;
    JsonUtils jsonUtils;
    Methods methods;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        methods = new Methods(this);
        methods.forceRTLIfSupported(getWindow());

        AdView adView = (AdView)findViewById(R.id.adView_profedit);
        adView.loadAd(new AdRequest.Builder().build());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        jsonUtils = new JsonUtils(this);

        toolbar = (Toolbar)findViewById(R.id.toolbar_proedit);
        toolbar.setTitle(getResources().getString(R.string.profile_edit));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        methods.setStatusColor(getWindow(),toolbar);

        button_update = (AppCompatButton)findViewById(R.id.button_prof_update);
        editText_name = (EditText)findViewById(R.id.editText_profedit_name);
        editText_email = (EditText)findViewById(R.id.editText_profedit_email);
        editText_phone = (EditText)findViewById(R.id.editText_profedit_phone);
        editText_pass = (EditText)findViewById(R.id.editText_profedit_password);
        editText_cpass = (EditText)findViewById(R.id.editText_profedit_cpassword);
        editText_city = (EditText)findViewById(R.id.editText_profedit_city);
        editText_address = (EditText)findViewById(R.id.editText_profedit_address);

        setProfileVar();

        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()) {
                    if(methods.isNetworkAvailable()) {
                        setVariables();
                        new UpdateUser().execute();
                    } else {
                        Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Boolean validate() {
        editText_name.setError(null);
        editText_email.setError(null);
        editText_cpass.setError(null);
        View focusView;
        if(editText_name.getText().toString().trim().isEmpty()) {
            Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.cannot_empty), Toast.LENGTH_SHORT).show();
            editText_name.setError(getString(R.string.cannot_empty));
            focusView = editText_name;
            focusView.requestFocus();
            return false;
        } else if(editText_email.getText().toString().trim().isEmpty()) {
            Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.email_empty), Toast.LENGTH_SHORT).show();
            editText_email.setError(getString(R.string.cannot_empty));
            focusView = editText_email;
            focusView.requestFocus();
            return false;
        } else if(!editText_pass.getText().toString().trim().equals(editText_cpass.getText().toString().trim())) {
            Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.pass_nomatch), Toast.LENGTH_SHORT).show();
            editText_cpass.setError(getString(R.string.pass_nomatch));
            focusView = editText_cpass;
            focusView.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private void setVariables() {
        name = editText_name.getText().toString();
        email = editText_email.getText().toString();
        phone = editText_phone.getText().toString();
        password = editText_pass.getText().toString();
        city = editText_city.getText().toString();
        address = editText_address.getText().toString();

        if(!password.equals("")) {
            methods.changeRemPass(false);
        }
    }

    private void updateArray() {
        Constant.userItem.setName(name);
        Constant.userItem.setEmail(email);
        Constant.userItem.setMobile(phone);
        Constant.userItem.setAddress(address);
        Constant.userItem.setCity(city);
    }

    class UpdateUser extends AsyncTask<String, String, String> {

        String suc = "";
        String msg = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {
            String nm = name.replace(" ","%20");
            String json_strng = JsonUtils.getJSONString(Constant.URL_PROFILE_EDIT_1+Constant.user_id+
                    Constant.URL_PROFILE_EDIT_2 + nm +
                    Constant.URL_PROFILE_EDIT_3 + email +
                    Constant.URL_PROFILE_EDIT_4 + password +
                    Constant.URL_PROFILE_EDIT_5 + phone +
                    Constant.URL_PROFILE_EDIT_6 + city +
                    Constant.URL_PROFILE_EDIT_7 + address);
            try {
                JSONObject json = new JSONObject(json_strng);
                JSONArray c = json.getJSONArray(Constant.TAG_ROOT);
                JSONObject s = c.getJSONObject(0);
                suc = s.getString(Constant.TAG_SUCCESS);
                msg = s.getString(Constant.TAG_MSG);

                return "1";
            } catch (JSONException e) {
                e.printStackTrace();
                return "0";
            } catch (Exception e) {
                e.printStackTrace();
                return "0";
            }
        }

        protected void onPostExecute(String success) {
            progressDialog.dismiss();
            setVariables();
            if(success.equals("1")) {
                if (suc.equals("1")) {
                    updateArray();
                    Constant.isUpdate = true;
                    finish();
                    Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.update_prof_succ), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.email_already_regis), Toast.LENGTH_SHORT).show();
                }
            } else if(success.equals("0")) {
                Toast.makeText(ProfileEditActivity.this, getResources().getString(R.string.update_prof_not_succ), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setProfileVar() {
        editText_name.setText(Constant.userItem.getName());
        editText_phone.setText(Constant.userItem.getMobile());
        editText_email.setText(Constant.userItem.getEmail());
        editText_city.setText(Constant.userItem.getCity());
        editText_address.setText(Constant.userItem.getAddress());
    }
}

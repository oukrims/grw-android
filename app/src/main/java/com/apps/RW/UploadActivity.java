package com.apps.RW;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.apps.utils.Methods;

public class UploadActivity extends AppCompatActivity {

    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    Methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        methods = new Methods(this);
        toolbar = (Toolbar)findViewById(R.id.toolbar_upload);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.upload));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        methods.setStatusColor(getWindow(),toolbar);
        methods.forceRTLIfSupported(getWindow());

        viewPager = (ViewPager)findViewById(R.id.vp_upload);
        tabLayout = (TabLayout)findViewById(R.id.tabs_upload);
        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(3);

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new FragmentUploadWall();

                case 1:
                    return new FragmentUploadRingtone();
                default:
                    return new FragmentUploadWall();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.wallpaper);

                case 1:
                    return getResources().getString(R.string.ringtone);
//
                default:
                    return getResources().getString(R.string.wallpaper);
            }
        }
    }
}

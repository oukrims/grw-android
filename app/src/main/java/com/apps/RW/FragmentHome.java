package com.apps.RW;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.adapter.AdapterRingtone;
import com.apps.adapter.AdapterVideoPager;
import com.apps.adapter.AdapterWallpaper;
import com.apps.items.ItemGIF;
import com.apps.items.ItemRingtone;
import com.apps.items.ItemVideos;
import com.apps.items.ItemWallpaper;
import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;
import com.apps.utils.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.apps.utils.Constant.arrayList_gif;

public class FragmentHome extends Fragment {

    Methods methods;
    RecyclerView rv_wall, rv_ringtone;
/*
    ClickableViewPager viewPager;
*/

    AppCompatButton button_wallmore,button_ringmore,button_gifmore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);


      ////  textView_ringtone_wall = (TextView)rootView.findViewById(R.id.tv_recent_empty_ringtone);
/*
        textView_video_empty = (TextView)rootView.findViewById(R.id.tv_recent_empty_video);
*/
        button_wallmore = (AppCompatButton)rootView.findViewById(R.id.button_more_wall);
        button_ringmore = (AppCompatButton)rootView.findViewById(R.id.button_more_ring);
        button_gifmore = (AppCompatButton) rootView.findViewById(R.id.button_more_gifs);
/*
        button_videomore = (AppCompatButton)rootView.findViewById(R.id.button_more_video);
*/



        button_wallmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),WallpaperActivity.class);
                startActivity(intent);
            }
        });

        button_ringmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),RingtoneActivity.class);
                startActivity(intent);
            }
        });
        button_gifmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),ByCatWallActivity.class);
               // int realpos = getPosition(adapterCat.getID(position));
                intent.putExtra("cid","0");
                intent.putExtra("cname","gifs");
                startActivity(intent);
            }
        });

      /*  button_videomore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),VideosActivity.class);
                startActivity(intent);
            }
        });*/

//        rv_wall.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                Constant.arrayList_wall.clear();
//                Constant.arrayList_wall.addAll(arrayList_wallpaper);
//                Intent intent = new Intent(getActivity(),DetailWallpaper.class);
//                intent.putExtra("pos",position);
//                startActivity(intent);
//            }
//        }));

        /*rv_ringtone.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Constant.arrayList_ring.clear();
                Constant.arrayList_ring.addAll(arrayList_ringtone);
                Intent intent = new Intent(getActivity(),DetailRingtone.class);
                intent.putExtra("pos",position);
                startActivity(intent);
            }
        }));*/

        /*viewPager.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Constant.arrayList_video.clear();
                Constant.arrayList_video.addAll(arrayList_videos);
                Intent intent = new Intent(getActivity(),DetailVideos.class);
                intent.putExtra("pos",position);
                startActivity(intent);
            }
        });*/

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        super.onPrepareOptionsMenu(menu);
    }



    @Override
    public void onResume() {

        super.onResume();
    }
}

package com.apps.RW;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.adapter.AdapterLatestWallpaper;
import com.apps.items.ItemWallpaper;
import com.apps.utils.Constant;
import com.apps.utils.EndlessRecyclerViewScrollListener;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentLatestWall extends Fragment {

    Methods methods;
    RecyclerView recyclerView;
    GridLayoutManager layout;
    AdapterLatestWallpaper adapterLatestWallpaper;
    ArrayList<ItemWallpaper> arrayList_wallpaper;
    ProgressDialog progressDialog;
    int viewpager_pos = 0;
    SearchView searchView;
    String json;
    Boolean isOver = false;

    public FragmentLatestWall newInstance(int pos) {
        FragmentLatestWall fragment = new FragmentLatestWall();
        Bundle bundle = new Bundle();
        bundle.putInt("pos", pos);
        fragment.setArguments(bundle);
        viewpager_pos = pos;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_latest_wall, container, false);

        methods = new Methods(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        Bundle bundle = getArguments();
        viewpager_pos = bundle.getInt("pos");

        arrayList_wallpaper = new ArrayList<>();
        adapterLatestWallpaper = new AdapterLatestWallpaper(getActivity(), arrayList_wallpaper);

        layout = new GridLayoutManager(getActivity(), 2);
//        layout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                return adapterLatestWallpaper.isHeader(position) ? layout.getSpanCount() : 1;
//            }
//        });
        recyclerView = (RecyclerView) v.findViewById(R.id.rv_latest_wall);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layout);

        if (methods.isNetworkAvailable()) {
            new LoadLatestWall().execute();
        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
        }

//        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
////				if((position+1)%4!=0) {
////					showInter(position);
////				}
////                showInter(position);
//                Constant.arrayList_wall.clear();
//                Constant.arrayList_wall.addAll(arrayList_wallpaper);
//                Intent intent = new Intent(getActivity(), DetailWallpaper.class);
//                int realpos = getPosition(adapterLatestWallpaper.getID(position));
//                intent.putExtra("pos", realpos);
//                startActivity(intent);
//            }
//        }));

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layout) {
            @Override
            public void onLoadMore(int p, int totalItemsCount) {
                if(!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new LoadLatestWall().execute();
                        }
                    }, 2000);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_more_data), Toast.LENGTH_SHORT).show();
                }
            }
        });

        setHasOptionsMenu(true);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            if (searchView.isIconified()) {
                recyclerView.setAdapter(adapterLatestWallpaper);
                adapterLatestWallpaper.notifyDataSetChanged();
            } else {
                adapterLatestWallpaper.getFilter().filter(s);
                adapterLatestWallpaper.notifyDataSetChanged();
            }
            return true;
        }
    };

    private class LoadLatestWall extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            if (arrayList_wallpaper.size() == 0) {
                progressDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                if(arrayList_wallpaper.size() == 0) {
                    if (viewpager_pos == 0) {
                        json = JsonUtils.getJSONString(Constant.URL_LATEST_WALLPAPER);
                    } else {
                        json = JsonUtils.getJSONString(Constant.URL_POPULAR_WALLPAPER);
                    }
                }
                JSONObject mainJson = new JSONObject(json);
                JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);

                if(jsonArray.length()<=arrayList_wallpaper.size()+Constant.dataLoadNumber) {
                    isOver = true;
                }

                int a = arrayList_wallpaper.size();
                int b = a + Constant.dataLoadNumber;

                for (int i = a; i < b; i++) {
                    JSONObject objJson = jsonArray.getJSONObject(i);

                    String id = objJson.getString(Constant.TAG_ID);
                    String cid = objJson.getString(Constant.TAG_CAT_ID);
                    String user_name = objJson.getString(Constant.TAG_USER_NAME);
                    String cname = objJson.getString(Constant.TAG_CAT_NAME);
                    String image = objJson.getString(Constant.TAG_IMAGE_B).replace(" ", "%20");
                    String image_small = objJson.getString(Constant.TAG_IMAGE_S).replace(" ", "%20");
                    String tags = objJson.getString(Constant.TAG_TAGS);
                    String rate_avg = objJson.getString(Constant.TAG_RATE_AVG);
                    if (rate_avg.equals("")) {
                        rate_avg = "0";
                    }
//                    String total_rate = objJson.getString(Constant.TAG_TOTAL_RATE);
                    String total_views = objJson.getString(Constant.TAG_TOTAL_VIEWS);
                    String total_download = objJson.getString(Constant.TAG_TOTAL_DOWNLOADS);
                    if(!cid.equals("0")){
                        ItemWallpaper objItem = new ItemWallpaper(id, cid, user_name, cname, image, image_small, tags, rate_avg, total_views, total_download);
                        arrayList_wallpaper.add(objItem);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
                isOver = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if(arrayList_wallpaper.size()<(Constant.dataLoadNumber+1)) {
                recyclerView.setAdapter(adapterLatestWallpaper);
            } else {
                adapterLatestWallpaper.notifyDataSetChanged();
            }

        }
    }

    @Override
    public void onResume() {
        if(adapterLatestWallpaper != null && arrayList_wallpaper.size()>0) {
            adapterLatestWallpaper.notifyDataSetChanged();
        }
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser) {
            if(adapterLatestWallpaper != null && arrayList_wallpaper.size()>0) {
                adapterLatestWallpaper.notifyDataSetChanged();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }
}
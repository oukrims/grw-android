package com.apps.RW;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.items.UserItem;
import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentProfile extends Fragment {

    Methods methods;
    TextView textView_name,textView_email,textView_mobile, textView_city, textView_address, textView_notlog;
    LinearLayout ll_mobile, ll_city, ll_address;
    ImageView imageView_name,imageView_email,imageView_phone;
    View view_phone, view_city, view_address;
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        methods = new Methods(getActivity());

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        textView_name = (TextView)rootView.findViewById(R.id.textView_prof_fname);
        textView_email = (TextView)rootView.findViewById(R.id.textView_prof_email);
        textView_mobile = (TextView)rootView.findViewById(R.id.textView_prof_mobile);
        textView_notlog = (TextView)rootView.findViewById(R.id.textView_notlog);
        textView_city = (TextView)rootView.findViewById(R.id.textView_prof_city);
        textView_address = (TextView)rootView.findViewById(R.id.textView_prof_address);

        ll_mobile = (LinearLayout)rootView.findViewById(R.id.ll_prof_phone);
        ll_city = (LinearLayout)rootView.findViewById(R.id.ll_prof_city);
        ll_address = (LinearLayout)rootView.findViewById(R.id.ll_prof_address);

        imageView_name = (ImageView)rootView.findViewById(R.id.imageView_prof_name);
        imageView_email = (ImageView)rootView.findViewById(R.id.imageView_prof_email);
        imageView_phone = (ImageView)rootView.findViewById(R.id.imageView_prof_mobile);

        view_phone= (View)rootView.findViewById(R.id.view_prof_phone);
        view_city= (View)rootView.findViewById(R.id.view_prof_city);
        view_address= (View)rootView.findViewById(R.id.view_prof_address);

        if(!Constant.user_id.equals("")) {
            if (methods.isNetworkAvailable()) {
                new LoadUser().execute();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
            }
        } else {
            textView_notlog.setVisibility(View.VISIBLE);
            ((MainActivity)getContext()).getSupportActionBar().setTitle(getResources().getString(R.string.profile));
        }

        setHasOptionsMenu(true);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_profile,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_profile_edit:
                if(!Constant.user_id.equals("")) {
                    Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.not_log), Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class LoadUser extends AsyncTask<String, String, String> {

        String suc = "";
        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            JSONObject json = JsonUtils.makeHttpRequest(Constant.URL_PROFILE + Constant.user_id, "GET", params);
            try {
//                JSONObject json = new JSONObject(json_strng);
                JSONArray c = json.getJSONArray(Constant.TAG_ROOT);
                JSONObject s = c.getJSONObject(0);
                String name = s.getString(Constant.TAG_NAME);
                String email = s.getString(Constant.TAG_USER_EMAIL);
                String mobile = s.getString(Constant.TAG_USER_PHONE);
                String city = s.getString(Constant.TAG_USER_CITY);
                String address = s.getString(Constant.TAG_USER_ADDRESS);
                suc = s.getString(Constant.TAG_SUCCESS);

                Constant.userItem = new UserItem(Constant.user_id,name,email,mobile,city,address);

                return "1";
            } catch (JSONException e) {
                e.printStackTrace();
                return "0";
            } catch (Exception e) {
                e.printStackTrace();
                return "0";
            }
        }

        protected void onPostExecute(String success) {
            progressDialog.dismiss();
            if(success.equals("0")) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_server), Toast.LENGTH_SHORT).show();
            } else {
                if(!suc.equals("1")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_user_found), Toast.LENGTH_SHORT).show();
                }
            }
            try {
                setVariables();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setVariables() {

        textView_name.setText(Constant.userItem.getName());
        textView_mobile.setText(Constant.userItem.getMobile());
        textView_email.setText(Constant.userItem.getEmail());
        textView_city.setText(Constant.userItem.getCity());
        textView_address.setText(Constant.userItem.getAddress());

        if(!Constant.userItem.getMobile().trim().isEmpty()) {
            ll_mobile.setVisibility(View.VISIBLE);
            view_phone.setVisibility(View.VISIBLE);
        }

        if(!Constant.userItem.getCity().trim().isEmpty()) {
            ll_city.setVisibility(View.VISIBLE);
            view_city.setVisibility(View.VISIBLE);
        }

        if(!Constant.userItem.getAddress().trim().isEmpty()) {
            ll_address.setVisibility(View.VISIBLE);
            view_address.setVisibility(View.VISIBLE);
        }
        
        textView_notlog.setVisibility(View.GONE);
        ((MainActivity)getContext()).getSupportActionBar().setTitle(Constant.userItem.getName());
    }

    @Override
    public void onResume() {
        if(Constant.isUpdate) {
        Constant.isUpdate = false;
        setVariables();
    }
        super.onResume();
    }
}

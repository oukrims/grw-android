package com.apps.RW;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.ExtendedViewPager;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;
import com.apps.utils.TouchImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.executor.FifoPriorityThreadPoolExecutor;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.bumptech.glide.GlideBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DetailWallpaper extends AppCompatActivity {

    Methods methods;
    DBHelper dbHelper;
    int pos;
    Toolbar toolbar;

    ExtendedViewPager viewPager;
    LinearLayout ll_download,ll_share,ll_rating,ll_fav, ll_hideShow, ll_hide, ll_play;
    TextView textView_uploadedby, textView_downloads, textView_cat, textView_tags;
    Boolean isFav = false;
    ImageView imageView_fav, imageView_hideshow,set_btn;
    RatingBar rating;
    Dialog dialog;
    int height = 0;
    int Media;
    InterstitialAd mInterstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_wallpaper);

        dbHelper = new DBHelper(this);
        loadInterAd();

        pos = getIntent().getIntExtra("pos",0);
        Media = getIntent().getIntExtra("media",1);
        toolbar = (Toolbar) findViewById(R.id.toolbar_wall_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.wallpaper));
        methods = new Methods(this);
        methods.forceRTLIfSupported(getWindow());
        methods.setStatusColor(getWindow(),toolbar);

        viewPager = (ExtendedViewPager)findViewById(R.id.viewPager_wall_details);
        Log.d("Set as" ,"umage type " + Media);

        loadViewed(pos);
        isFav = dbHelper.isFav(Constant.arrayList_wall.get(pos).getId(),"wall");


        CustomPagerAdapter adapter = new CustomPagerAdapter(this);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pos);

        ll_hideShow = (LinearLayout)findViewById(R.id.ll_hideshow);
        ll_hide = (LinearLayout)findViewById(R.id.ll_hide);
        ll_download = (LinearLayout)findViewById(R.id.ll_download);
        ll_share = (LinearLayout)findViewById(R.id.ll_share);
        ll_rating = (LinearLayout)findViewById(R.id.ll_rating);
        ll_fav = (LinearLayout)findViewById(R.id.ll_fav);
        ll_play = (LinearLayout)findViewById(R.id.ll_play_option);
        textView_cat = (TextView)findViewById(R.id.tv_wall_details_cat);
        textView_uploadedby = (TextView)findViewById(R.id.tv_wall_details_uploadby);
        textView_downloads = (TextView)findViewById(R.id.tv_wall_details_downloads);
        textView_tags = (TextView)findViewById(R.id.tv_wall_details_tags);
        imageView_fav = (ImageView) findViewById(R.id.iv_wall_fav);
        imageView_hideshow = (ImageView) findViewById(R.id.iv_hidesho_wall);
        rating = (RatingBar) findViewById(R.id.rating_wall_details);

        ll_play.setVisibility(View.GONE);
        setFavImage();

        setTexts();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
                loadViewed(position);
                setTexts();
                isFav = dbHelper.isFav(Constant.arrayList_wall.get(pos).getId(),"wall");
                setFavImage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ll_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("download");
                (new SaveTask(DetailWallpaper.this)).execute(Constant.arrayList_wall.get(pos).getImageBig().replace(" ", "%20"));
            }
        });

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("share");
//                (new ShareTask(DetailWallpaper.this)).execute(Constant.arrayList_wall.get(pos).getImageBig().replace(" ", "%20"));
            }
        });

        ll_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("rate");
//                openRateDialog();
            }
        });





        ll_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFav) {
                    dbHelper.addFavWall(Constant.arrayList_wall.get(pos));
                    isFav = true;
                    Toast.makeText(DetailWallpaper.this, getString(R.string.added_fav), Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.removeFav(Constant.arrayList_wall.get(pos).getId(),"wallpaper");
                    isFav = false;
                    Toast.makeText(DetailWallpaper.this, getString(R.string.removed_fav), Toast.LENGTH_SHORT).show();
                }
                setFavImage();
            }
        });

        imageView_hideshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                height = ll_hide.getHeight();
                if(ll_hide.getVisibility() == View.GONE) {
                    Animation animation = new TranslateAnimation(0,0,height,0);
                    animation.setDuration(300);
                    animation.setInterpolator(new OvershootInterpolator(1.0f));
                    animation.setFillEnabled(true);
                    ll_hideShow.startAnimation(animation);
                    ll_hide.setVisibility(View.VISIBLE);
                    imageView_hideshow.setImageDrawable(getResources().getDrawable(android.R.drawable.arrow_down_float));
//                    ll_hide.setVisibility(View.VISIBLE);
                } else {
                    Animation animation = new TranslateAnimation(0,0,0,height);
                    animation.setDuration(300);
                    animation.setInterpolator(new OvershootInterpolator(1.0f));
                    animation.setFillEnabled(true);
                    ll_hideShow.startAnimation(animation);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            ll_hide.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    imageView_hideshow.setImageDrawable(getResources().getDrawable(android.R.drawable.arrow_up_float));
//                    ll_hide.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if(Media == 1) {
            MenuInflater menuInflater = getMenuInflater();

            menuInflater.inflate(R.menu.menu_setwall, menu);
        }
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_setwallpaper:
                (new SetTask(DetailWallpaper.this)).execute(Constant.arrayList_wall.get(pos).getImageBig().replace(" ", "%20"));

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        private CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return Constant.arrayList_wall.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view ==  object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.layout_viewpager_wall, container, false);
            final TouchImageView img = new TouchImageView(container.getContext());
            //img.setTag(position);

            if(Constant.arrayList_wall.get(position).getImageBig().endsWith("gif")){
                Glide.with(mContext)
                        .load(Constant.arrayList_wall.get(position).getImageBig())
                        .asGif()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.drawable.placeholder_long)
                        .into(img);

                getSupportActionBar().setTitle("Gifs");

               toolbar.getMenu().clear();



            }
            else {
                Glide.with(mContext)
                        .load(Constant.arrayList_wall.get(position).getImageBig())
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.drawable.placeholder_long)
                        .into(img);

            }
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            container.addView(itemView);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    private void setTexts() {
        textView_cat.setText(Constant.arrayList_wall.get(pos).getCName());
        textView_downloads.setText(Constant.arrayList_wall.get(pos).getTotalDownload());
        textView_uploadedby.setText(Constant.arrayList_wall.get(pos).getUserName());
        textView_tags.setText(Constant.arrayList_wall.get(pos).getTags());
        rating.setRating(Float.parseFloat(Constant.arrayList_wall.get(pos).getRateAvg()));
    }
    private class SetTask extends AsyncTask<String , String , String> {
        private Context context;
        private ProgressDialog pDialog;
        URL myFileUrl;
        Bitmap bmImg = null;
        File file;
        Boolean isDownload = false;

        private SetTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

            pDialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.download_image));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            try {
                myFileUrl = new URL(args[0]);
                //myFileUrl1 = args[0];

                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bmImg = BitmapFactory.decodeStream(is);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            try {

                String path = myFileUrl.getPath();
                String idStr = path.substring(path.lastIndexOf('/') + 1);
                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File (filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER);
                dir.mkdirs();
                Log.d("path","path : "+filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER);
                String fileName = idStr;
                Log.d("filename","path : "+idStr);

                file = new File(dir, fileName);
                if(!file.exists()) {
                    FileOutputStream fos = new FileOutputStream(file);
                    bmImg.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                    fos.flush();
                    fos.close();

                    MediaScannerConnection.scanFile(DetailWallpaper.this, new String[]{file.getAbsolutePath()},
                            null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {

                                }
                            });

                    Intent setAs = new Intent(Intent.ACTION_ATTACH_DATA);
                    setAs.setType("image/jpg");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File f = new File(filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER);
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        setAs.setDataAndType(Uri.parse("content:///sdcard/"+Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER+"/"+fileName),
                                "image/jpg");
                        setAs.putExtra("mimeType", "image/jpg");
                        startActivity(Intent.createChooser(setAs, "Set Image As"));
                    }else{
                        setAs.setDataAndType(Uri.parse("file:///sdcard/"+Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER+"/"+fileName),
                                "image/jpg");
                        setAs.putExtra("mimeType", "image/jpg");
                        startActivity(Intent.createChooser(setAs, "Set Image As"));
                    }


                    //loadDownloaded(pos);


                } else {
                    Intent setAs = new Intent(Intent.ACTION_ATTACH_DATA);
                    setAs.setType("image/jpg");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File f = new File(filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER);
                    try {
                        f.createNewFile();
                        FileOutputStream fos = new FileOutputStream(f);
                        fos.write(bytes.toByteArray());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("set wall"," file "+Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER+""+fileName);
                    setAs.setDataAndType(Uri.parse("file:///sdcard/"+Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER+""+fileName+""),
                            "image/jpg");
                    setAs.putExtra("mimeType", "image/jpg");
                    startActivity(Intent.createChooser(setAs, "Set Image As"));
                    isDownload = true;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String args) {
            if(isDownload) {
                //  Toast.makeText(DetailWallpaper.this, "wallpaper is set", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(DetailWallpaper.this, getResources().getString(R.string.already_downloaded), Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();
        }
    }
    private class SaveTask extends AsyncTask<String , String , String> {
        private Context context;
        private ProgressDialog pDialog;
        URL myFileUrl;
        Bitmap bmImg = null;
        File file;
        Boolean isDownload = false;

        private SaveTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

            pDialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.download_image));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            try {
                myFileUrl = new URL(args[0]);
                //myFileUrl1 = args[0];

                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bmImg = BitmapFactory.decodeStream(is);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            try {

                String path = myFileUrl.getPath();
                String idStr = path.substring(path.lastIndexOf('/') + 1);
                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File (filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER);
                dir.mkdirs();
                String fileName = idStr;
                file = new File(dir, fileName);
                if(!file.exists()) {
                    isDownload = true;
                    FileOutputStream fos = new FileOutputStream(file);
                    bmImg.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                    fos.flush();
                    fos.close();

                    MediaScannerConnection.scanFile(DetailWallpaper.this, new String[]{file.getAbsolutePath()},
                            null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {

                                }
                            });

                    loadDownloaded(pos);
                } else {
                    isDownload = false;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String args) {
            if(isDownload) {
                Toast.makeText(DetailWallpaper.this, getResources().getString(R.string.image_save_success), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DetailWallpaper.this, getResources().getString(R.string.already_downloaded), Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();
        }
    }

    public class ShareTask extends AsyncTask<String , String , String>
    {
        private Context context;
        private ProgressDialog pDialog;
        URL myFileUrl;
        Bitmap bmImg = null;
        File file ;

        public ShareTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

            pDialog = new ProgressDialog(context,AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.please_wait));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            try {

                myFileUrl = new URL(args[0]);
                //myFileUrl1 = args[0];

                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bmImg = BitmapFactory.decodeStream(is);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            try {

                String path = myFileUrl.getPath();
                String idStr = path.substring(path.lastIndexOf('/') + 1);
                File filepath = getExternalCacheDir();
                File dir = new File (filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER);
                dir.mkdirs();
                String fileName = idStr;
                file = new File(dir, fileName);
                FileOutputStream fos = new FileOutputStream(file);
                bmImg.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String args) {
            // TODO Auto-generated method stub

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getAbsolutePath()));
            startActivity(Intent.createChooser(share, getResources().getString(R.string.share_image)));
            pDialog.dismiss();
        }
    }

    private void loadViewed(int pos) {
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_WALLPAPER_SINGLE + Constant.arrayList_wall.get(pos).getId(),String.valueOf(pos),"views");
        }
    }

    private void loadDownloaded(int pos) {
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_WALLPAPER_DOWNLOADS + Constant.arrayList_wall.get(pos).getId(),String.valueOf(pos),"downloads");
        }
    }

    private void loadRating(int pos, float rating) {
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_RATING_1 + Constant.arrayList_wall.get(pos).getId() +
                    Constant.URL_RATING_2 + "wallpaper" + Constant.URL_RATING_3 + String.valueOf(rating) +
                    Constant.URL_RATING_4 + deviceId,String.valueOf(pos),"rating");
        }
    }

    private	class MyTask extends AsyncTask<String, Void, String> {

        String type = "";
        String rate = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String json = JsonUtils.getJSONString(params[0]);
            type = params[2];

            if(type.equals("rating")) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG_ROOT);
                    JSONObject jsonObj = jsonArray.getJSONObject(0);
                    rate = jsonObj.getString("rate_avg");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return params[1];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            int p = Integer.parseInt(result);
            switch (type) {
                case "downloads":
                    int download = Integer.parseInt(Constant.arrayList_wall.get(p).getTotalDownload());
                    Constant.arrayList_wall.get(p).setTotalDownloads("" + (download + 1));
                    dbHelper.updateViewWall(Constant.arrayList_wall.get(pos).getId(), Constant.arrayList_wall.get(p).getTotalDownload(),"downloads");
                    textView_downloads.setText(Constant.arrayList_wall.get(pos).getTotalDownload());
                    break;
                case "views":
                    int tot = Integer.parseInt(Constant.arrayList_wall.get(p).getTotalViews());
                    Constant.arrayList_wall.get(p).setTotalViews("" + (tot + 1));
                    dbHelper.updateViewWall(Constant.arrayList_wall.get(pos).getId(), Constant.arrayList_wall.get(p).getTotalViews(),"views");
                    break;
                case "rating":
//                    float rating = Float.parseFloat(Constant.arrayList_wall.get(p).getRateAvg());
                    if(!rate.equals("")) {
                        Constant.arrayList_wall.get(p).setRateAvg(rate);
                        dbHelper.updateViewWall(Constant.arrayList_wall.get(pos).getId(), Constant.arrayList_wall.get(p).getRateAvg(), "rate");
                        Toast.makeText(DetailWallpaper.this, getResources().getString(R.string.rating_submit), Toast.LENGTH_SHORT).show();
                        rating.setRating(Float.parseFloat(rate));
                    } else {
                        Toast.makeText(DetailWallpaper.this, getResources().getString(R.string.already_rated), Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                    break;
            }
        }
    }

    public void setFavImage() {
        if(isFav) {
            imageView_fav.setImageResource(R.mipmap.heart_red_round_hover);
        } else {
            imageView_fav.setImageResource(R.mipmap.heart_red_round);
        }
    }

    private void openRateDialog() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new Dialog(DetailWallpaper.this,android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            dialog = new Dialog(DetailWallpaper.this);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_rating);

        final RatingBar ratingBar = (RatingBar)dialog.findViewById(R.id.rating_wall);
        ratingBar.setMax(5);
        ratingBar.setNumStars(5);
        AppCompatButton button_submit = (AppCompatButton) dialog.findViewById(R.id.button_submit_rating_wall);

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRating(pos, ratingBar.getRating());
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public void showInterAd(final String option) {

        Constant.adCount = Constant.adCount + 1;

        if(Constant.adCount % Constant.adShow == 0) {
            if(mInterstitial.isLoaded()) {
                mInterstitial.show();
                mInterstitial.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        click(option);
                        loadInterAd();
                        super.onAdClosed();
                    }
                });
            } else {
                click(option);
            }
        } else {
            click(option);
        }
    }

    public void loadInterAd() {
        mInterstitial = new InterstitialAd(DetailWallpaper.this);
        mInterstitial.setAdUnitId(getResources().getString(R.string.admob_intertestial_id));
        mInterstitial.loadAd(new AdRequest.Builder().build());
    }

    private void click(String option) {
        switch (option) {
            case "download":
                (new SaveTask(DetailWallpaper.this)).execute(Constant.arrayList_wall.get(pos).getImageBig().replace(" ", "%20"));
                break;
            case "share":
                (new ShareTask(DetailWallpaper.this)).execute(Constant.arrayList_wall.get(pos).getImageBig().replace(" ", "%20"));
                break;
            case "rate":
                openRateDialog();
                break;
            default:
                break;
        }
    }
}

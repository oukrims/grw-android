package com.apps.RW;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    AppCompatEditText editText_name, editText_email, editText_pass, editText_cpass, editText_phone;
    Button button_register;
    TextView textView_login;
    Methods methods;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        methods = new Methods(this);
        methods.setStatusColor(getWindow(),null);
        methods.forceRTLIfSupported(getWindow());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        editText_name = (AppCompatEditText)findViewById(R.id.et_regis_name);
        editText_email = (AppCompatEditText)findViewById(R.id.et_regis_email);
        editText_pass = (AppCompatEditText)findViewById(R.id.et_regis_password);
        editText_cpass = (AppCompatEditText)findViewById(R.id.et_regis_cpassword);
        editText_phone = (AppCompatEditText)findViewById(R.id.et_regis_phone);

        button_register = (Button)findViewById(R.id.button_register);
        textView_login = (TextView)findViewById(R.id.tv_login_regis);

        textView_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()) {
                    if(methods.isNetworkAvailable()) {
                        new LoadRegister().execute(editText_name.getText().toString(),editText_email.getText().toString(),editText_pass.getText().toString(),editText_phone.getText().toString());
                    } else {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private Boolean validate(){
        if(editText_name.getText().toString().trim().isEmpty()) {
            editText_name.setError(getResources().getString(R.string.enter_name));
            editText_name.requestFocus();
            return false;
        } else if(editText_email.getText().toString().trim().isEmpty()) {
            editText_email.setError(getResources().getString(R.string.enter_email));
            editText_email.requestFocus();
            return false;
        } else if (!isEmailValid(editText_email.getText().toString().trim())) {
            editText_email.setError(getString(R.string.error_invalid_email));
            editText_email.requestFocus();
            return false;
        } else if(editText_pass.getText().toString().trim().isEmpty()) {
            editText_pass.setError(getResources().getString(R.string.enter_password));
            editText_pass.requestFocus();
            return false;
        } else if(editText_cpass.getText().toString().trim().isEmpty()) {
            editText_cpass.setError(getResources().getString(R.string.enter_cpassword));
            editText_cpass.requestFocus();
            return false;
        } else if(!editText_pass.getText().toString().equals(editText_cpass.getText().toString())) {
            editText_cpass.setError(getResources().getString(R.string.pass_nomatch));
            editText_cpass.requestFocus();
            return false;
        } else if(editText_phone.getText().toString().trim().isEmpty()) {
            editText_phone.setError(getResources().getString(R.string.enter_phone));
            editText_phone.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private class LoadRegister extends AsyncTask<String,String,String> {

        String msg = "";
        String suc = "";

        @Override
        protected void onPreExecute() {
            try {
                progressDialog.show();
            } catch(Exception e) {
                e.printStackTrace();
            }
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String name = strings[0];
            String email = strings[1];
            String pass = strings[2];
            String phone = strings[3];

            String json_strng = JsonUtils.getJSONString(Constant.URL_REGISTER_1 + name + Constant.URL_REGISTER_2 + email + Constant.URL_REGISTER_3 + pass + Constant.URL_REGISTER_4 + phone);
            try {
                JSONObject jsonObject = new JSONObject(json_strng);
                JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG_ROOT);
                JSONObject obj = jsonArray.getJSONObject(0);

                msg = obj.getString(Constant.TAG_MSG);
                suc = obj.getString(Constant.TAG_SUCCESS);

                return "1";

            } catch (JSONException e) {
                e.printStackTrace();
                return "0";
            } catch (Exception ee) {
                ee.printStackTrace();
                return "0";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            if(s.equals("0")) {
            } else {
                if(msg.contains("already")) {
//                    progressHUD.dismissWithFailure(getResources().getString(R.string.error));
                    editText_email.setError(getResources().getString(R.string.email_already_regis));
                    editText_email.requestFocus();
                } else if(msg.contains("Invalid email format")) {
//                    progressHUD.dismissWithFailure(getResources().getString(R.string.error));
                    editText_email.setError(getResources().getString(R.string.error_invalid_email));
                    editText_email.requestFocus();
                } else {
//                    progressHUD.dismissWithSuccess(getResources().getString(R.string.success));
                    Toast.makeText(RegisterActivity.this, getString(R.string.register_success), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
            super.onPostExecute(s);
        }
    }
}

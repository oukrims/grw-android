package com.apps.RW;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.ExtendedViewPager;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DetailRingtone extends AppCompatActivity {

    Methods methods;
    DBHelper dbHelper;
    int pos;
    Toolbar toolbar;
    ExtendedViewPager viewPager;
    LinearLayout ll_download, ll_share, ll_rating, ll_fav;
    RelativeLayout rl_cancel_play;
    TextView textView_uploadedby, textView_downloads, textView_cat, textView_tags, textView_title, textView_duration;
    Boolean isFav = false, isPlay = false, isFirstPlay = true;
    ImageView imageView_fav, imageView_play;
    RatingBar rating;
    Dialog dialog;
    MediaPlayer mediaPlayer;
    Handler seekHandler = new Handler();
    InterstitialAd mInterstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_ringtone);

        registerReceiver(onCallIncome, new IntentFilter("android.intent.action.PHONE_STATE"));
        loadInterAd();

        dbHelper = new DBHelper(this);

        pos = getIntent().getIntExtra("pos", 0);
        toolbar = (Toolbar) findViewById(R.id.toolbar_ring_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.ringtone));
        methods = new Methods(this);
        methods.forceRTLIfSupported(getWindow());
        methods.setStatusColor(getWindow(),toolbar);

        viewPager = (ExtendedViewPager) findViewById(R.id.viewPager_ring_details);
        initMediaPLayer();

        loadViewed(pos);
        isFav = dbHelper.isFav(Constant.arrayList_ring.get(pos).getId(),"ring");

        CustomPagerAdapter adapter = new CustomPagerAdapter(this);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pos);

        rl_cancel_play = (RelativeLayout) findViewById(R.id.rl_loading);
        ll_download = (LinearLayout) findViewById(R.id.ll_download);
        ll_share = (LinearLayout) findViewById(R.id.ll_share);
        ll_rating = (LinearLayout) findViewById(R.id.ll_rating);
        ll_fav = (LinearLayout) findViewById(R.id.ll_fav);
        textView_uploadedby = (TextView) findViewById(R.id.tv_ring_details_uploadby);
        textView_downloads = (TextView) findViewById(R.id.tv_ring_details_downloads);
        textView_tags = (TextView) findViewById(R.id.tv_ring_details_tags);
        textView_title = (TextView) findViewById(R.id.tv_title);
        textView_duration = (TextView) findViewById(R.id.tv_duration_ring);
        imageView_fav = (ImageView) findViewById(R.id.iv_ring_fav);
        imageView_play = (ImageView) findViewById(R.id.iv_play_ring);
        rating = (RatingBar) findViewById(R.id.rating_ring_details);

        textView_duration.setText(Constant.arrayList_ring.get(pos).getDuration());

        setFavImage();

        setTexts();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    pos = position;
                    loadViewed(position);
                    setTexts();
                    isFav = dbHelper.isFav(Constant.arrayList_ring.get(pos).getId(), "ring");
                    setFavImage();
                    isFirstPlay = true;
                    isPlay = false;
                    hideShowBuffer(false);
                    textView_duration.setText(Constant.arrayList_ring.get(pos).getDuration());
                    imageView_play.setImageDrawable(getResources().getDrawable(R.drawable.play_orange));
                    mediaPlayer.pause();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ll_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("download");
//                download();
            }
        });

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("share");
//                (new ShareTask(DetailRingtone.this)).execute(Constant.arrayList_ring.get(pos).getImageBig().replace(" ", "%20"));
            }
        });

        ll_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("rate");
//                openRateDialog();
            }
        });

        ll_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFav) {
                    dbHelper.addFavRing(Constant.arrayList_ring.get(pos));
                    isFav = true;
                    Toast.makeText(DetailRingtone.this, getString(R.string.added_fav), Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.removeFav(Constant.arrayList_ring.get(pos).getId(), "ringtone");
                    isFav = false;
                    Toast.makeText(DetailRingtone.this, getString(R.string.removed_fav), Toast.LENGTH_SHORT).show();
                }
                setFavImage();
            }
        });

        imageView_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isPlay) {
                    if(isFirstPlay) {
                        playAudio();
                    } else {
                        play();
                    }
                } else {
                    pause();
                }
            }
        });

//        rl_cancel_play.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hideShowBuffer(false);
//                mediaPlayer.stop();
//                mediaPlayer.release();
//                isFirstPlay = false;
//            }
//        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

            MenuInflater menuInflater = getMenuInflater();

             return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        private CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return Constant.arrayList_ring.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = mLayoutInflater.inflate(R.layout.layout_viewpager_video, container, false);
            final ImageView img = new ImageView(container.getContext());
            img.setScaleType(ImageView.ScaleType.FIT_XY);
            img.setTag(position);
            Picasso
                    .with(DetailRingtone.this)
                    .load(Constant.arrayList_ring.get(position).getImageBig())
                    .placeholder(R.drawable.placeholder_long)
                    .into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            container.addView(itemView);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    private void setTexts() {
//        textView_cat.setText(Constant.arrayList_ring.get(pos).getCname());
        textView_downloads.setText(Constant.arrayList_ring.get(pos).getTotalDownload());
        textView_uploadedby.setText(Constant.arrayList_ring.get(pos).getUserName());
        textView_tags.setText(Constant.arrayList_ring.get(pos).getTags());
try{      rating.setRating(Float.parseFloat(Constant.arrayList_ring.get(pos).getRateAvg()));}catch(Exception e){Log.d("ring rateing",e.getMessage());}
    textView_title.setText(Constant.arrayList_ring.get(pos).getTitle());
    }
    private class SetTask extends AsyncTask<String , String , String> {
        private Context context;
        private ProgressDialog pDialog;
        URL myFileUrl;
        Bitmap bmImg = null;
        File file;
        Boolean isDownload = false;

        private SetTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

            pDialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.download_image));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            //pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            try {
                myFileUrl = new URL(args[0]);
                //myFileUrl1 = args[0];

                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bmImg = BitmapFactory.decodeStream(is);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            try {

                String path = myFileUrl.getPath();
                String idStr = path.substring(path.lastIndexOf('/') + 1);
                File filepath = Environment.getExternalStorageDirectory();
                File dir = new File (filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE);
                dir.mkdirs();
                Log.d("path","path : "+filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE);
                String fileName = idStr;
                Log.d("filename","path : "+idStr);

                file = new File(dir, fileName);
                if(!file.exists()) {
                    FileOutputStream fos = new FileOutputStream(file);
                    bmImg.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                    fos.flush();
                    fos.close();

                    MediaScannerConnection.scanFile(DetailRingtone.this, new String[]{file.getAbsolutePath()},
                            null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {

                                }
                            });

                    Intent setAs = new Intent(Intent.ACTION_ATTACH_DATA);
                    setAs.setType("image/jpg");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File f = new File(filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE);
                    try {
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(bytes.toByteArray());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    setAs.setDataAndType(Uri.parse("content:///sdcard/"+Constant.DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE+"/"+fileName),
                            "audio/*");
                    setAs.putExtra("mimeType", "audio/*");
                    startActivity(Intent.createChooser(setAs, "Set Ringtone As"));
                    //loadDownloaded(pos);


                } else {
                    Intent setAs = new Intent(Intent.ACTION_ATTACH_DATA);
                    setAs.setType("audio/*");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    File f = new File(filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER);
                    try {
                        f.createNewFile();
                        FileOutputStream fos = new FileOutputStream(f);
                        fos.write(bytes.toByteArray());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.d("set wall"," file "+Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER+""+fileName);
                    setAs.setDataAndType(Uri.parse("file:///sdcard/"+Constant.DOWNLOAD_SDCARD_FOLDER_PATH_WALLPAPER+""+fileName+""),
                            "audio/*");
                    setAs.putExtra("mimeType", "audio/*");
                    startActivity(Intent.createChooser(setAs, "Set Ringtone As"));
                    isDownload = true;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String args) {
            if(isDownload) {
                //  Toast.makeText(DetailWallpaper.this, "wallpaper is set", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(DetailWallpaper.this, getResources().getString(R.string.already_downloaded), Toast.LENGTH_SHORT).show();
            }
            pDialog.dismiss();
        }
    }
    public class ShareTask extends AsyncTask<String, String, String> {
        private Context context;
        private ProgressDialog pDialog;
        URL myFileUrl;
        Bitmap bmImg = null;
        File file;

        public ShareTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

            super.onPreExecute();

            pDialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.please_wait));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            try {

                myFileUrl = new URL(args[0]);
                //myFileUrl1 = args[0];

                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                bmImg = BitmapFactory.decodeStream(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {

                String path = myFileUrl.getPath();
                String idStr = path.substring(path.lastIndexOf('/') + 1);
                File filepath = getExternalCacheDir();
                File dir = null;
                if (filepath != null) {
                    dir = new File(filepath.getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE);
                }
                dir.mkdirs();
                String fileName = idStr;
                file = new File(dir, fileName);
                FileOutputStream fos = new FileOutputStream(file);
                bmImg.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String args) {
            // TODO Auto-generated method stub

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.title) + " - " + Constant.arrayList_ring.get(pos).getTitle() + "\n\n" + Constant.arrayList_ring.get(pos).getUrl());
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getAbsolutePath()));
            startActivity(Intent.createChooser(share, getResources().getString(R.string.share_ringtone)));
            pDialog.dismiss();
        }
    }

    private void loadViewed(int pos) {
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_RINGTONE_SINGLE + Constant.arrayList_ring.get(pos).getId(), String.valueOf(pos), "views");
        }
    }

    private void loadDownloaded(int pos) {
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_RINGTONE_DOWNLOADS + Constant.arrayList_ring.get(pos).getId(), String.valueOf(pos), "downloads");
        }
    }

    private void loadRating(int pos, float rating) {
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_RATING_1 + Constant.arrayList_ring.get(pos).getId() +
                    Constant.URL_RATING_2 + "ringtone" + Constant.URL_RATING_3 + String.valueOf(rating) +
                    Constant.URL_RATING_4 + deviceId, String.valueOf(pos), "rating");
        }
    }

    private class MyTask extends AsyncTask<String, Void, String> {

        String type = "";
        String rate = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String json = JsonUtils.getJSONString(params[0]);
            type = params[2];

            if (type.equals("rating")) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG_ROOT);
                    JSONObject jsonObj = jsonArray.getJSONObject(0);
                    rate = jsonObj.getString("rate_avg");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return params[1];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            int p = Integer.parseInt(result);
            switch (type) {
                case "downloads":
                    int download = Integer.parseInt(Constant.arrayList_ring.get(p).getTotalDownload());
                    Constant.arrayList_ring.get(p).setTotalDownloads("" + (download + 1));
                    dbHelper.updateViewRing(Constant.arrayList_ring.get(pos).getId(), Constant.arrayList_ring.get(p).getTotalDownload(), "downloads");
                    textView_downloads.setText(Constant.arrayList_ring.get(pos).getTotalDownload());
                    break;
                case "views":
                    try{int tot = Integer.parseInt(Constant.arrayList_ring.get(p).getTotalViews());}catch(Exception e){ Log.d("totla vies",e.getMessage());}
                    dbHelper.updateViewRing(Constant.arrayList_ring.get(pos).getId(), Constant.arrayList_ring.get(p).getTotalViews(), "views");
                    break;
                case "rating":
//                    float rating = Float.parseFloat(Constant.arrayList_ring.get(p).getRateAvg());
                    if (!rate.equals("")) {
                        Constant.arrayList_ring.get(p).setRateAvg(rate);
                        dbHelper.updateViewRing(Constant.arrayList_ring.get(pos).getId(), Constant.arrayList_ring.get(p).getRateAvg(), "rate");
                        Toast.makeText(DetailRingtone.this, getResources().getString(R.string.rating_submit), Toast.LENGTH_SHORT).show();
                        rating.setRating(Float.parseFloat(rate));
                    } else {
                        Toast.makeText(DetailRingtone.this, getResources().getString(R.string.already_rated), Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                    break;
            }
        }
    }

    public void setFavImage() {
        if (isFav) {
            imageView_fav.setImageResource(R.mipmap.heart_red_round_hover);
        } else {
            imageView_fav.setImageResource(R.mipmap.heart_red_round);
        }
    }

    private void openRateDialog() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new Dialog(DetailRingtone.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            dialog = new Dialog(DetailRingtone.this);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_rating);

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.rating_wall);
        ratingBar.setMax(5);
        ratingBar.setNumStars(5);
        AppCompatButton button_submit = (AppCompatButton) dialog.findViewById(R.id.button_submit_rating_wall);

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRating(pos, ratingBar.getRating());
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void download() {
        File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Constant.DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE);

        if(!root.exists()) {
            root.mkdirs();
        }

        File file = new File(root,Constant.arrayList_ring.get(pos).getTitle()+".mp3");

        if(!file.exists()) {
            String url = Constant.arrayList_ring.get(pos).getUrl();
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDescription(getResources().getString(R.string.downloading) + " - " + Constant.arrayList_ring.get(pos).getTitle());
            request.setTitle(Constant.arrayList_ring.get(pos).getTitle());
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            request.setDestinationInExternalPublicDir(Constant.DOWNLOAD_SDCARD_FOLDER_PATH_RINGTONE, Constant.arrayList_ring.get(pos).getTitle() + ".mp3");

            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);

            loadDownloaded(pos);
        } else {
            Toast.makeText(DetailRingtone.this, getResources().getString(R.string.already_downloaded), Toast.LENGTH_SHORT).show();
        }
    }

    private void initMediaPLayer() {
        mediaPlayer = new MediaPlayer();
//        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mediaPlayer) {
//            }
//        });
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
//                progressDialog.dismiss();
                rl_cancel_play.setEnabled(false);
                hideShowBuffer(false);
                play();
                seekUpdation();
                rl_cancel_play.setEnabled(true);
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                isFirstPlay = true;
                isPlay = false;
                imageView_play.setImageDrawable(getResources().getDrawable(R.drawable.play_orange));
            }
        });
    }

    private void playAudio() {
        new LoadSong().execute();
    }

    class LoadSong extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            hideShowBuffer(true);
//            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... a) {
            try {
                mediaPlayer.reset();
                String s = Constant.arrayList_ring.get(pos).getUrl().replace(" ","%20");
                mediaPlayer.setDataSource(DetailRingtone.this, Uri.parse(s));
                Log.e("url::",s);
                mediaPlayer.prepareAsync();
                isFirstPlay = false;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private void play() {
        isPlay = true;
        mediaPlayer.start();
        imageView_play.setImageDrawable(getResources().getDrawable(R.drawable.pause_orange));
    }

    private void pause() {
        isPlay = false;
        mediaPlayer.pause();
        imageView_play.setImageDrawable(getResources().getDrawable(R.drawable.play_orange));
    }

    private void seekUpdation() {
        if(mediaPlayer.isPlaying()) {
            textView_duration.setText(miliTomin(mediaPlayer.getCurrentPosition()));
        }
        seekHandler.postDelayed(run, 500);
    }

    Runnable run = new Runnable() {
        @Override public void run() {
            seekUpdation();
        }
    };

    public static String miliTomin(int t)
    {
        String time="0:00";
        int temp;
        temp = t/1000;
        int count = 0;
        if(temp < 60) {
            if(temp < 10) {
                time = count + ":0"+temp;
            } else {
                time = count+":"+temp;
            }
        } else {
            while (temp >= 60) {
                temp = temp - 60;
                count++;
            }
            if(temp < 10) {
                time = count + ":0"+temp;
            } else {
                time = count+":"+temp;
            }
        }

        return time;
    }

    BroadcastReceiver onCallIncome = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            if(isPlay) {
                if(a.equals(TelephonyManager.EXTRA_STATE_RINGING) || a.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    mediaPlayer.pause();
                } else if (a.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    mediaPlayer.start();
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        unregisterReceiver(onCallIncome);
        super.onDestroy();
    }
    public void showInterAd(final String option) {

        Constant.adCount = Constant.adCount + 1;

        if(Constant.adCount % Constant.adShow == 0) {
            if(mInterstitial.isLoaded()) {
                mInterstitial.show();
                mInterstitial.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        click(option);
                        loadInterAd();
                        super.onAdClosed();
                    }
                });
            } else {
                click(option);
            }
        } else {
            click(option);
        }
    }

    public void loadInterAd() {
        mInterstitial = new InterstitialAd(DetailRingtone.this);
        mInterstitial.setAdUnitId(getResources().getString(R.string.admob_intertestial_id));
        mInterstitial.loadAd(new AdRequest.Builder().build());
    }

    private void click(String option) {
        switch (option) {
            case "download":
                download();
                break;
            case "share":
                (new ShareTask(DetailRingtone.this)).execute(Constant.arrayList_ring.get(pos).getImageBig().replace(" ", "%20"));
                break;
            case "rate":
                openRateDialog();
                break;
            default:
                break;
        }
    }

    private void hideShowBuffer(Boolean show) {
        if(show) {
            rl_cancel_play.setVisibility(View.VISIBLE);
            imageView_play.setVisibility(View.GONE);
        } else {
            imageView_play.setVisibility(View.VISIBLE);
            rl_cancel_play.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        if(isPlay) {
            mediaPlayer.pause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if(isPlay) {
            mediaPlayer.start();
        }
        super.onResume();
    }
}
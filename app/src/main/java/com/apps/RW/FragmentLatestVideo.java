package com.apps.RW;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.adapter.AdapterLatestVideo;
import com.apps.items.ItemVideos;
import com.apps.utils.Constant;
import com.apps.utils.EndlessRecyclerViewScrollListener;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentLatestVideo extends Fragment {

    Methods methods;
    RecyclerView recyclerView;
    LinearLayoutManager layout;
    AdapterLatestVideo adapterLatestVideo;
    ArrayList<ItemVideos> arrayList_videos;
    ProgressDialog progressDialog;
    int viewpager_pos = 0;
    SearchView searchView;
    String json;
    Boolean isOver = false;

    public FragmentLatestVideo newInstance(int pos) {
        FragmentLatestVideo fragment = new FragmentLatestVideo();
        Bundle bundle = new Bundle();
        bundle.putInt("pos", pos);
        fragment.setArguments(bundle);
        viewpager_pos = pos;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_latest_wall,container,false);

        methods = new Methods(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        Bundle bundle = getArguments();
        viewpager_pos = bundle.getInt("pos");

        arrayList_videos = new ArrayList<>();
        adapterLatestVideo = new AdapterLatestVideo(getActivity(),arrayList_videos);

        layout = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView)v.findViewById(R.id.rv_latest_wall);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layout);

        if(methods.isNetworkAvailable()) {
            new LoadLatestWall().execute();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
        }

//        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
////				if((position+1)%4!=0) {
////					showInter(position);
////				}
////                showInter(position);
//                Constant.arrayList_video.clear();
//                Constant.arrayList_video.addAll(arrayList_videos);
//                Intent intent = new Intent(getActivity(),DetailVideos.class);
//                int realpos = getPosition(adapterLatestVideo.getID(position));
//                intent.putExtra("pos",realpos);
//                startActivity(intent);
//            }
//        }));

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layout) {
            @Override
            public void onLoadMore(int p, int totalItemsCount) {
                if(!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new LoadLatestWall().execute();
                        }
                    }, 2000);
                } else {
                    Toast.makeText(getActivity(), "No more data", Toast.LENGTH_SHORT).show();
                }
            }
        });

        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            if (searchView.isIconified()) {
                recyclerView.setAdapter(adapterLatestVideo);
                adapterLatestVideo.notifyDataSetChanged();
            } else {
                adapterLatestVideo.getFilter().filter(s);
                adapterLatestVideo.notifyDataSetChanged();
            }
            return true;
        }
    };

    private class LoadLatestWall extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            if(arrayList_videos.size() == 0) {
                progressDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                if(arrayList_videos.size() == 0) {
                    if (viewpager_pos == 0) {
                        json = JsonUtils.getJSONString(Constant.URL_LATEST_VIDEOS);
                    } else {
                        json = JsonUtils.getJSONString(Constant.URL_POPULAR_VIDEOS);
                    }
                }
                JSONObject mainJson = new JSONObject(json);
                JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);

                if(jsonArray.length()<=arrayList_videos.size()+Constant.dataLoadNumber) {
                    isOver = true;
                }

                int a = arrayList_videos.size();
                int b = a + Constant.dataLoadNumber;

                for (int i = a; i < b; i++) {
                    JSONObject objJson = jsonArray.getJSONObject(i);

                    String id = objJson.getString(Constant.TAG_ID);
                    String cid = objJson.getString(Constant.TAG_CAT_ID);
                    String user_name = objJson.getString(Constant.TAG_USER_NAME);
                    String cname = objJson.getString(Constant.TAG_CAT_NAME);
                    String type = objJson.getString(Constant.TAG_VIDEO_TYPE);
                    String title = objJson.getString(Constant.TAG_VIDEO_TITLE);
                    String url = objJson.getString(Constant.TAG_VIDEO_URL);
                    String video_id = objJson.getString(Constant.TAG_VIDEO_ID);
                    String duration = objJson.getString(Constant.TAG_VIDEO_DURATION);
//                    String desc = objJson.getString(Constant.TAG_VIDEO_DESC);
                    String image = objJson.getString(Constant.TAG_VIDEO_IMAGE_B).replace(" ","%20");
                    String image_small = objJson.getString(Constant.TAG_VIDEO_IMAGE_S).replace(" ","%20");
                    String tags = objJson.getString(Constant.TAG_TAGS);
                    String rate_avg = objJson.getString(Constant.TAG_RATE_AVG);
                    if(rate_avg.equals("")) {
                        rate_avg = "0";
                    }
//                    String total_rate = objJson.getString(Constant.TAG_TOTAL_RATE);
                    String total_views = objJson.getString(Constant.TAG_TOTAL_VIEWS);

                    ItemVideos itemVideos = new ItemVideos(id,cid,user_name,cname,type,title,url,video_id,duration,image,image_small,tags,rate_avg,total_views);
                    arrayList_videos.add(itemVideos);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                isOver = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if(arrayList_videos.size()<(Constant.dataLoadNumber+1)) {
                recyclerView.setAdapter(adapterLatestVideo);
            } else {
                adapterLatestVideo.notifyDataSetChanged();
            }

        }
    }

    @Override
    public void onResume() {
        if(adapterLatestVideo != null && arrayList_videos.size()>0) {
            adapterLatestVideo.notifyDataSetChanged();
        }
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser) {
            if(adapterLatestVideo != null && arrayList_videos.size()>0) {
                adapterLatestVideo.notifyDataSetChanged();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }
}
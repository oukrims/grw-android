package com.apps.RW;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.json.JSONArray;
import org.json.JSONObject;

public class FragmentFeedBack extends Fragment {

    Methods methods;
    AppCompatButton button;
    AppCompatEditText editText_add, editText_name, editText_email;
    ProgressDialog progressDialog;
    View focusView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_feedback,container,false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        button = (AppCompatButton)v.findViewById(R.id.button_fb_submit);
        editText_add = (AppCompatEditText)v.findViewById(R.id.et_fb_address);
        editText_email = (AppCompatEditText)v.findViewById(R.id.et_fb_email);
        editText_name = (AppCompatEditText)v.findViewById(R.id.et_fb_name);

        methods = new Methods(getActivity());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(methods.isNetworkAvailable()) {
                    if(validate()) {
                        new LoadFeedBack().execute();
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
                }
            }
        });

        setHasOptionsMenu(true);

        return v;
    }

    private class LoadFeedBack extends AsyncTask<String,String,String> {

        String name, email, address, msg;

        @Override
        protected void onPreExecute() {
            name = editText_name.getText().toString();
            email = editText_email.getText().toString();
            address = editText_add.getText().toString();
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String json = JsonUtils.getJSONString(Constant.URL_FEEDBACK_1 + name + Constant.URL_FEEDBACK_2 + email + Constant.URL_FEEDBACK_3 + address);
                JSONObject mainJson = new JSONObject(json);
                JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objJson = jsonArray.getJSONObject(i);

                    msg = objJson.getString("MSG");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();
            if(msg.contains("Feedback succesfully submit")){
                Toast.makeText(getActivity(), getResources().getString(R.string.feedback_success), Toast.LENGTH_SHORT).show();
                editText_add.setText("");
                editText_email.setText("");
                editText_name.setText("");
            }

        }
    }

    private Boolean validate() {
        if(editText_name.getText().toString().trim().isEmpty()) {
            editText_name.setError(getString(R.string.cannot_empty));
            focusView = editText_name;
            return false;
        } else if(editText_email.getText().toString().trim().isEmpty()) {
            editText_email.setError(getString(R.string.cannot_empty));
            focusView = editText_email;
            return false;
        } else if(editText_add.getText().toString().trim().isEmpty()) {
            editText_add.setError(getString(R.string.cannot_empty));
            focusView = editText_add;
            return false;
        } else {
            return true;
        }
    }
}
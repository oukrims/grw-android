package com.apps.RW;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.adapter.AdapterCat;
import com.apps.items.ItemCat;
import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;
import com.apps.utils.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentCategoryVideo extends Fragment {

    Methods methods;
    ArrayList<ItemCat> arrayList;
    AdapterCat adapterCat;
    RecyclerView recyclerView;
    GridLayoutManager layout;
    ProgressDialog progressDialog;
    SearchView searchView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cat,container,false);

        methods = new Methods(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        arrayList = new ArrayList<ItemCat>();

        layout = new GridLayoutManager(getActivity(),2);
        recyclerView = (RecyclerView)v.findViewById(R.id.rv_cat);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layout);

        if(methods.isNetworkAvailable()) {
            new LoadCat().execute();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
        }

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            Intent intent = new Intent(getContext(),ByCatVideoActivity.class);
            int realpos = getPosition(adapterCat.getID(position));
            intent.putExtra("cid",arrayList.get(realpos).getId());
            intent.putExtra("cname",arrayList.get(realpos).getName());
            startActivity(intent);
            }
        }));

        setHasOptionsMenu(true);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            if (searchView.isIconified()) {
                recyclerView.setAdapter(adapterCat);
                adapterCat.notifyDataSetChanged();
            } else {
                adapterCat.getFilter().filter(s);
                adapterCat.notifyDataSetChanged();
            }
            return true;
        }
    };

    private class LoadCat extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                String json = JsonUtils.getJSONString(Constant.URL_CAT_VIDEOS);
                JSONObject mainJson = new JSONObject(json);
                JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);

                for(int i=0; i<jsonArray.length(); i++) {
                    JSONObject format = jsonArray.getJSONObject(i);
                    String id = format.getString(Constant.TAG_CAT_ID);
                    String name = format.getString(Constant.TAG_CAT_NAME);
                    String image_big = format.getString(Constant.TAG_CAT_IMAGE);
                    String image_small = format.getString(Constant.TAG_CAT_IMAGE_THUMB);

                    ItemCat itemCat = new ItemCat(id,name,image_big,image_small);
                    arrayList.add(itemCat);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            adapterCat = new AdapterCat(getActivity(),arrayList);
            recyclerView.setAdapter(adapterCat);

        }
    }

    private int getPosition(String id) {
        int count = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (id.equals(arrayList.get(i).getId())) {
                count = i;
                break;
            }
        }
        return count;
    }
}
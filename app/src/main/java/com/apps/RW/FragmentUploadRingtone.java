package com.apps.RW;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class FragmentUploadRingtone extends Fragment {

    Methods methods;
    ProgressDialog progressDialog, progress;
    Spinner spinner,spinner_ring_type;
    EditText editText, editText_url, editText_title, editText_duration;
    AppCompatButton button_browse, button_submit, button_ringtone;
    TextView textView_ringpath;
    ImageView imageView;
    ArrayList<String> arrayList_cat, arrayList_catid, arrayList_type;
    private int PICK_RING_REQUEST = 1, PICK_THUMB_REQUEST = 2;
    String suc = "", ringPath="", imagePath="", cat_id, wall_name, ringDuration, ringTitle, ringURL, ringType="local",tags;
    HttpURLConnection conn;
    private int serverResponseCode = 0;
    File sourceFile;
    int totalSize;
    Bitmap bitmap_upload;
    LinearLayout ll_url, ll_ring;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upload_ring, container, false);

        methods = new Methods(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(100);

        progress = new ProgressDialog(getActivity());
        progress.setMessage(getActivity().getResources().getString(R.string.loading));
        progress.setCancelable(false);

        ll_url = (LinearLayout) v.findViewById(R.id.ll_upload_ring_url);
        ll_ring = (LinearLayout) v.findViewById(R.id.ll_upload_ring);
        button_ringtone = (AppCompatButton) v.findViewById(R.id.button_upload_ringtone_browse);
        button_browse = (AppCompatButton) v.findViewById(R.id.button_upload_ring_browse);
        button_submit = (AppCompatButton) v.findViewById(R.id.button_upload_ring_submit);
        textView_ringpath = (TextView) v.findViewById(R.id.button_upload_ringtone_name);

        imageView = (ImageView) v.findViewById(R.id.iv_upload_wall_submit);

        arrayList_cat = new ArrayList<>();
        arrayList_catid = new ArrayList<>();
        arrayList_type = new ArrayList<>();
        arrayList_cat.add("main");
        arrayList_catid.add("0");

        editText = (EditText) v.findViewById(R.id.et_upload_wall);
        editText_url = (EditText) v.findViewById(R.id.et_upload_ring_url);
        editText_title = (EditText) v.findViewById(R.id.et_upload_ring_title);
        editText_duration = (EditText) v.findViewById(R.id.et_upload_ring_duration);
        spinner = (Spinner) v.findViewById(R.id.spinner_upload_ringcat);
        spinner_ring_type = (Spinner) v.findViewById(R.id.spinner_upload_ring_type);

        arrayList_type.add("Local");
        arrayList_type.add("Server");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner, arrayList_type);
        spinner_ring_type.setAdapter(adapter);

        if (methods.isNetworkAvailable()) {
            new LoadCat().execute();
        } else {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
        }

        button_ringtone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent_upload = new Intent();
                intent_upload.setType("audio/*");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent_upload,PICK_RING_REQUEST);
            }
        });

        button_browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_THUMB_REQUEST);
            }
        });

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Constant.isLogged) {
                    if(methods.isNetworkAvailable()) {
                        if(imagePath.equals("")) {
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.select_thumb), Toast.LENGTH_SHORT).show();
                        } else {
                            if (ringType.equals("local")) {
                                new Upload().execute(imagePath);
                            } else {
                                if (!editText_url.getText().toString().trim().isEmpty() && !editText_title.getText().toString().trim().isEmpty() && !editText_duration.getText().toString().trim().isEmpty()) {
                                    new Upload().execute(imagePath);
                                } else {
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.fill_details), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    methods.clickLogin();
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat_id = arrayList_catid.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_ring_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0) {
                    ringType = "local";
                    ll_url.setVisibility(View.GONE);
                    ll_ring.setVisibility(View.VISIBLE);
                } else {
                    ringType = "server_url";
                    ll_url.setVisibility(View.VISIBLE);
                    ll_ring.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return v;
    }

    private class LoadCat extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            progress.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                String json = JsonUtils.getJSONString(Constant.URL_CAT_RING);
                JSONObject mainJson = new JSONObject(json);
                JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject format = jsonArray.getJSONObject(i);
                    String name = format.getString(Constant.TAG_CAT_NAME);
                    String id = format.getString(Constant.TAG_CAT_ID);

                    arrayList_cat.add(name);
                    arrayList_catid.add(id);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progress.isShowing()) {
                progress.dismiss();
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner, arrayList_cat);
            spinner.setAdapter(adapter);

            if(arrayList_catid.size() > 0) {
                cat_id = arrayList_catid.get(0);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_RING_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            ringPath = getFilePathFromContentUri(uri,getActivity().getContentResolver());
            ringDuration = getDuration(ringPath);
            ringTitle = new File(ringPath).getName();
            ringTitle = ringTitle.substring(0, ringTitle.lastIndexOf("."));
            textView_ringpath.setText(ringPath);
        } else if (requestCode == PICK_THUMB_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            imagePath = methods.getPathImage(uri);

            try {
                bitmap_upload = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                imageView.setImageBitmap(bitmap_upload);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getDuration(String filePath) {
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(filePath);

        String songDuration =
                metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

        long duration = Long.parseLong(songDuration );
        String seconds = String.valueOf((duration  % 60000) / 1000);
        if(Integer.parseInt(seconds)<10) {
            seconds = "0"+seconds;
        }

        String minutes = String.valueOf(duration / 60000);

        String time = "";

        time = minutes + ":" + seconds;
        // close object
        metaRetriever.release();

        return time;
    }

    public String getPath(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;

    }

    private String getFilePathFromContentUri(Uri uri, ContentResolver contentResolver) {
//        String filePath;
//        String[] filePathColumn = {MediaStore.MediaColumns.DATA};
//
//        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
//        cursor.moveToFirst();
//
//        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//        filePath = cursor.getString(columnIndex);
//        cursor.close();
//        return filePath;

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                String filePath = "";
                String wholeID = DocumentsContract.getDocumentId(uri);

                // Split at colon, use second item in the array
                String id = wholeID.split(":")[1];

                String[] column = {MediaStore.Audio.Media.DATA};

                // where id is equal to
                String sel = MediaStore.Audio.Media._ID + "=?";

                Cursor cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);

                int columnIndex = cursor.getColumnIndex(column[0]);

                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
                return filePath;
            } else {

                if (uri == null) {
                    return null;
                }
                // try to retrieve the image from the media store first
                // this will only work for images selected from gallery
                String[] projection = {MediaStore.Audio.Media.DATA};
                Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
                if (cursor != null) {
                    int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                    cursor.moveToFirst();
                    return cursor.getString(column_index);
                }
                // this is our fallback here
                return uri.getPath();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (uri == null) {
                return null;
            }
            // try to retrieve the image from the media store first
            // this will only work for images selected from gallery
            String[] projection = {MediaStore.Audio.Media.DATA};
            Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            // this is our fallback here
            return uri.getPath();
        }
    }

    public class Upload extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            progressDialog.setProgress(0);
            progressDialog.show();
            wall_name = "";
            tags = editText.getText().toString();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            uploadImage(strings[0]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(ringType.equals("local")) {
                new UploadRingtone().execute();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.upload_success), Toast.LENGTH_LONG).show();
                editText.setText("");
                ringDuration = "";
                ringTitle = "";
                ringURL="";
                imagePath = "";
                imageView.setImageBitmap(null);
                textView_ringpath.setText("");
                editText_url.setText("");
                editText_title.setText("");
                editText_duration.setText("");
                progressDialog.dismiss();
            }
            super.onPostExecute(s);
        }
    }

    public int uploadImage(String sourceFileUri) {

        final String upLoadServerUri = Constant.URL_UPLOAD_RING_THUMB;

        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        String as[] = null;

        as = new String[1];
        as[0] = sourceFile.toString();


        if (!sourceFile.isFile()) {
            Log.e("uploadFile", getResources().getString(R.string.source_file_not_found));
            progressDialog.dismiss();
            return 0;
        }
        try { // open a URL connection to the Servlet

            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(upLoadServerUri);
            conn = (HttpURLConnection) url.openConnection(); // Open a HTTP  connection to  the URL
//            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            //*********filename first is php base filename
            conn.setRequestProperty("ringtone_thumbnail", sourceFile.getAbsolutePath());

            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            //********filename first is php base filename
            dos.writeBytes("Content-Disposition: form-data; name=\"ringtone_thumbnail\";filename=\"" + sourceFile.getAbsolutePath() + "\"" + lineEnd);
            dos.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available(); // create a buffer of  maximum size

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + '\n');
            }
            String jsonString = stringBuilder.toString();
            Log.e("json", jsonString);
            JSONObject jsonObj = new JSONObject(jsonString);


            try {
                JSONArray jsonArray = jsonObj.getJSONArray(Constant.TAG_ROOT);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                wall_name = jsonObject.getString("ringtone_thumbnail_name");
                String success = jsonObject.getString("success");

                if(!ringType.equals("local")) {
                    ringURL = editText_url.getText().toString();
                    ringTitle = editText_title.getText().toString();
                    ringDuration = editText_duration.getText().toString();
                    uploadData(success);
                } else {
                    ringURL = "";
                }


//                if (success.equals("1")) {
//                    List<NameValuePair> data2 = new ArrayList<NameValuePair>();
//                    JSONObject jObj = JsonUtils.makeHttpRequest(Constant.URL_UPLOAD_WALL_RESP_1 + cat_id + Constant.URL_UPLOAD_WALL_RESP_2 + Constant.user_id + Constant.URL_UPLOAD_WALL_RESP_3 + Constant.userItem.getName() + Constant.URL_UPLOAD_WALL_RESP_4 + wall_name + Constant.URL_UPLOAD_WALL_RESP_5 + tags, "GET", data2);
//                    JSONArray jsonArr = jObj.getJSONArray(Constant.TAG_ROOT);
//                    JSONObject Obj = jsonArr.getJSONObject(0);
//
////                    String meessage = Obj.getString("msg");
//                    suc = Obj.getString("success");
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dos.flush();
            dos.close();
        }
        catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), "Exception : " + e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("Upload file to server", "Exception : " + e.getMessage(), e);
        }
        conn.disconnect();
        return serverResponseCode;
    }

//    private String uploadFile() {
//        String responseString = null;
//
//        HttpClient httpclient = new DefaultHttpClient();
//        HttpPost httppost = new HttpPost(Constant.URL_RINGTONE_DOWNLOADS);
//
//        try {
//
//            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
//                    new AndroidMultiPartEntity.ProgressListener() {
//
//                        @Override
//                        public void transferred(long num) {
////                            publishProgress((int) ((num / (float) totalSize) * 100));
//                        }
//                    });
//
//
//            File sourceFile = new File(ringPath);
//            entity.addPart("image", new FileBody(sourceFile));
//
//            // Extra parameters if you want to pass to server
////            entity.addPart("website",
////                    new StringBody("www.androidhive.info"));
////            entity.addPart("email", new StringBody("abc@gmail.com"));
//
////            totalSize = entity.getContentLength();
//            httppost.setEntity(entity);
//
//            // Making server call
//            HttpResponse response = httpclient.execute(httppost);
//            HttpEntity r_entity = response.getEntity();
//
//            int statusCode = response.getStatusLine().getStatusCode();
//            if (statusCode == 200) {
//                // Server response
//                responseString = EntityUtils.toString(r_entity);
//            } else {
//                responseString = "Error occurred! Http Status Code: "
//                        + statusCode;
//            }
//
//        } catch (ClientProtocolException e) {
//            responseString = e.toString();
//        } catch (IOException e) {
//            responseString = e.toString();
//        }
//
//        return responseString;
//
//    }

    private class UploadRingtone extends AsyncTask<String, Integer, String> {

//        ProgressDialog loading;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sourceFile = new File(ringPath);
            totalSize = (int)sourceFile.length();
//            loading = ProgressDialog.show(getActivity(), "Uploading Image", "Please wait...", true, true);
//            loading.show();
        }

        @Override
        protected String doInBackground(String... params) {

            final String upLoadServerUri = Constant.URL_UPLOAD_RING;
            JSONObject jsonObj = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File sourceFile = new File(ringPath);

            String as[] = null;

            as = new String[1];
            as[0] = sourceFile.toString();


            if (!sourceFile.isFile()) {
                Log.e("uploadFile", getResources().getString(R.string.source_file_not_found));
                progressDialog.dismiss();

            }

            try {
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(upLoadServerUri);
                conn = (HttpURLConnection) url.openConnection(); // Open a HTTP  connection to  the URL
//            conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                //*********filename first is php base filename
                conn.setRequestProperty("mp3_local", sourceFile.getAbsolutePath());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                //********filename first is php base filename
                dos.writeBytes("Content-Disposition: form-data; name=\"mp3_local\";filename=\"" + sourceFile.getAbsolutePath() + "\"" + lineEnd);
                dos.writeBytes(lineEnd);

                bytesAvailable = fileInputStream.available(); // create a buffer of  maximum size

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
//                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                int progress = 0;
                while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                    dos.write(buffer, 0, bufferSize);
                    progress += bytesRead;
                    Log.e("progress",""+(progress*100)/bytesAvailable);
                    publishProgress((progress*100)/bytesAvailable);
//                    bytesAvailable = fileInputStream.available();
//                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
//                    bytesRead = fileInputStream.read(buffer);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + '\n');
                }
                String jsonString = stringBuilder.toString();

                jsonObj = new JSONObject(jsonString);
                Log.e("json", jsonString);

                JSONArray jsonArray = jsonObj.getJSONArray(Constant.TAG_ROOT);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                ringURL = jsonObject.getString("ringtone_url");
                String success = jsonObject.getString("success");

            } catch (Exception e) {
                // Exception
                e.printStackTrace();
            } finally {
                if (conn != null) conn.disconnect();
            }

            try {

                JSONArray jsonArray = jsonObj.getJSONArray(Constant.TAG_ROOT);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                String success = jsonObject.getString("success");

                uploadData(success);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            progressDialog.setProgress((int) (progress[0]));
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            loading.dismiss();
            progressDialog.dismiss();
            if(suc.equals("1")) {
                Toast.makeText(getActivity(), getResources().getString(R.string.upload_success), Toast.LENGTH_LONG).show();
                editText.setText("");
                ringDuration = "";
                ringPath= "";
                ringTitle = "";
                ringURL="";
                imagePath = "";
                imageView.setImageBitmap(null);
                textView_ringpath.setText("");
                editText_url.setText("");
                editText_title.setText("");
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.error_uploading), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadData(String success) {
        try {
            if (success.equals("1")) {
                List<NameValuePair> data2 = new ArrayList<NameValuePair>();
                ringTitle = ringTitle.replace(" ","%20");

                DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                HttpPost httppost = new HttpPost(Constant.URL_UPLOAD_RING_RESP_1 + cat_id + Constant.URL_UPLOAD_RING_RESP_2 + Constant.user_id + Constant.URL_UPLOAD_RING_RESP_3 + Constant.userItem.getName() + Constant.URL_UPLOAD_RING_RESP_4 + ringType + Constant.URL_UPLOAD_RING_RESP_5 + ringTitle + Constant.URL_UPLOAD_RING_RESP_6 + wall_name + Constant.URL_UPLOAD_RING_RESP_7 + ringDuration + Constant.URL_UPLOAD_RING_RESP_8 + tags + Constant.URL_UPLOAD_RING_RESP_9 + ringURL);
// Depends on your web service
                httppost.setHeader("Content-type", "application/json");

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();
                    // json is UTF-8 by default
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();

                    String line = null;
                    while ((line = reader.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (Exception e) {
                    // Oops
                }
                finally {
                    try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
                }

//                JSONObject jObj = JsonUtils.makeHttpRequest(Constant.URL_UPLOAD_RING_RESP_1 + cat_id + Constant.URL_UPLOAD_RING_RESP_2 + Constant.user_id + Constant.URL_UPLOAD_RING_RESP_3 + Constant.userItem.getName() + Constant.URL_UPLOAD_RING_RESP_4 + ringType + Constant.URL_UPLOAD_RING_RESP_5 + ringTitle + Constant.URL_UPLOAD_RING_RESP_6 + wall_name + Constant.URL_UPLOAD_RING_RESP_7 + ringDuration + Constant.URL_UPLOAD_RING_RESP_8 + tags + Constant.URL_UPLOAD_RING_RESP_9 + ringURL, "GET", data2);
                JSONObject jObj = new JSONObject(result);
                JSONArray jsonArr = jObj.getJSONArray(Constant.TAG_ROOT);
                JSONObject Obj = jsonArr.getJSONObject(0);

                suc = Obj.getString("success");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
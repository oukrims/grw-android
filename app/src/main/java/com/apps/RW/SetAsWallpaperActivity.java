package com.apps.RW;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.apps.utils.Methods;
import com.edmodo.cropper.CropImageView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SetAsWallpaperActivity extends AppCompatActivity {

    private CropImageView mCropImageView;
    Methods methods;
    String url="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_as_wallpaper);

        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar_setaswall);
        toolbar.setTitle(getResources().getString(R.string.set_wallpaper));
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        methods = new Methods(this);
        methods.forceRTLIfSupported(getWindow());
        methods.setStatusColor(getWindow(),toolbar);

        Intent i=getIntent();
        url = i.getStringExtra("url");
        mCropImageView = (CropImageView)findViewById(R.id.CropImageView);

        mCropImageView.setImageBitmap(getBitmapFromURL(url));
    }

    public void setAsWallpaper(View view) throws IOException {
        (new SetWallpaperTask(SetAsWallpaperActivity.this)).execute("");
    }

    public class SetWallpaperTask extends AsyncTask<String , String , String>
    {
        private Context context;
        private ProgressDialog pDialog;
        Bitmap bmImg = null;

        public SetWallpaperTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pDialog = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
            pDialog.setMessage(getResources().getString(R.string.setting_wallpaper));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... args) {
            // TODO Auto-generated method stub

            return null;
        }


        @Override
        protected void onPostExecute(String args) {
            // TODO Auto-generated method stub
           // bmImg=mCropImageView.getCroppedImage();

           /* WallpaperManager wpm = WallpaperManager.getInstance(getApplicationContext()); // --The method context() is undefined for the type SetWallpaperTask
            try {
                wpm.setBitmap(bmImg);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/

            setBgIntent(url);
            pDialog.dismiss();
            Toast.makeText(SetAsWallpaperActivity.this, getResources().getString(R.string.wallpaper_set), Toast.LENGTH_SHORT).show();

            finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }
    private void setBgIntent (String src){
           /*URL url = new URL(src);
           HttpURLConnection connection = (HttpURLConnection) url.openConnection();
           connection.setDoInput(true);
           connection.connect();*/
           Uri uri = Uri.parse(src);

           Intent intent = new Intent(Intent.ACTION_ATTACH_DATA);
           intent.addCategory(Intent.CATEGORY_DEFAULT);
           intent.setDataAndType(uri, "image/jpeg");
           intent.putExtra("mimeType", "image/jpeg");
           startActivity(Intent.createChooser(intent, "Set as:"));


    }
}

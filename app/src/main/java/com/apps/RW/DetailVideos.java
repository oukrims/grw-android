package com.apps.RW;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;
import com.apps.utils.ScrollableViewPager;
import com.apps.vimeo.Vimeo;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DetailVideos extends AppCompatActivity {

    Methods methods;
    DBHelper dbHelper;
    int pos;
    Toolbar toolbar;
    ScrollableViewPager viewPager;
    LinearLayout ll_share, ll_rating, ll_fav, ll;
    TextView textView_uploadedby, textView_cat, textView_tags, textView_title, textView_duration;
    Boolean isFav = false;
    ImageView imageView_fav, imageView_play;
    RatingBar rating;
    Dialog dialog;
    ProgressDialog progressDialog;
    InterstitialAd mInterstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_videos);

        dbHelper = new DBHelper(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.playing));
        progressDialog.setMessage(getResources().getString(R.string.playing));
        progressDialog.setCancelable(false);

        loadInterAd();

        pos = getIntent().getIntExtra("pos", 0);
        toolbar = (Toolbar) findViewById(R.id.toolbar_video_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.videos));
        methods = new Methods(this);
        methods.forceRTLIfSupported(getWindow());
        methods.setStatusColor(getWindow(),toolbar);

        viewPager = (ScrollableViewPager) findViewById(R.id.viewPager_video_details);
        viewPager.setCanScroll(false);

        loadViewed(pos);
        isFav = dbHelper.isFav(Constant.arrayList_video.get(pos).getId(), "video");

        CustomPagerAdapter adapter = new CustomPagerAdapter(this);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pos);

        ll = (LinearLayout) findViewById(R.id.ll);
        ll_share = (LinearLayout) findViewById(R.id.ll_share_video);
        ll_rating = (LinearLayout) findViewById(R.id.ll_rating_video);
        ll_fav = (LinearLayout) findViewById(R.id.ll_fav_video);
        textView_cat = (TextView) findViewById(R.id.tv_video_details_cat);
        textView_uploadedby = (TextView) findViewById(R.id.tv_video_details_uploadby);
        textView_tags = (TextView) findViewById(R.id.tv_video_details_tags);
        textView_title = (TextView) findViewById(R.id.tv_title_video);
        textView_duration = (TextView) findViewById(R.id.tv_duration_video);
        imageView_fav = (ImageView) findViewById(R.id.iv_video_fav);
        imageView_play = (ImageView) findViewById(R.id.iv_play_video);
        rating = (RatingBar) findViewById(R.id.rating_videos_details);

        setFavImage();
        setTexts();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
                loadViewed(position);
                setTexts();
                isFav = dbHelper.isFav(Constant.arrayList_video.get(pos).getId(), "video");
                setFavImage();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                (new ShareTask(DetailVideos.this)).execute(Constant.arrayList_video.get(pos).getImageBig().replace(" ", "%20"));
                showInterAd("share");
//                methods.shareVideo(pos,Constant.arrayList_video.get(pos).getImageBig(),Constant.arrayList_video.get(pos).getTitle(),Constant.arrayList_video.get(pos).getUrl());
            }
        });

        ll_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("rate");
//                openRateDialog();
            }
        });

        ll_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFav) {
                    dbHelper.addFavVideo(Constant.arrayList_video.get(pos));
                    isFav = true;
                    Toast.makeText(DetailVideos.this, getString(R.string.added_fav), Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.removeFav(Constant.arrayList_video.get(pos).getId(),"video");
                    isFav = false;
                    Toast.makeText(DetailVideos.this, getString(R.string.removed_fav), Toast.LENGTH_SHORT).show();
                }
                setFavImage();
            }
        });

        imageView_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInterAd("play");
//                playVideo();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            ll.setVisibility(View.VISIBLE);
//            toolbar.setVisibility(View.VISIBLE);
//            showSystemUI(getWindow().getDecorView());
//
//            if(Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("youtube")) {
//                youtube.exitFullScreen();
//            }
//        } else {
//            ll.setVisibility(View.GONE);
//            toolbar.setVisibility(View.GONE);
//            hideSystemUI(getWindow().getDecorView());
//
//            if(Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("youtube")) {
//                youtube.enterFullScreen();
//            }
//        }
//        super.onConfigurationChanged(newConfig);
//    }

    private class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        private CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return Constant.arrayList_video.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view ==  object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            final View itemView = mLayoutInflater.inflate(R.layout.layout_viewpager_video, container, false);
            final ImageView img = (ImageView)itemView.findViewById(R.id.iv_video_detail);
            img.setScaleType(ImageView.ScaleType.FIT_XY);
            Picasso
                    .with(DetailVideos.this)
                    .load(Constant.arrayList_video.get(position).getImageBig())
                    .placeholder(R.drawable.placeholder_long)
                    .into(img);

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    private void setTexts() {
        textView_cat.setText(Constant.arrayList_video.get(pos).getCName());
        textView_uploadedby.setText(Constant.arrayList_video.get(pos).getUserName());
        textView_tags.setText(Constant.arrayList_video.get(pos).getTags());
        textView_title.setText(Constant.arrayList_video.get(pos).getTitle());
        textView_duration.setText(Constant.arrayList_video.get(pos).getDuration());
        rating.setRating(Float.parseFloat(Constant.arrayList_video.get(pos).getRateAvg()));
    }

    private void loadViewed(int pos) {
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_VIDEOS_SINGLE + Constant.arrayList_video.get(pos).getId(),String.valueOf(pos),"views");
        }
    }

    private void loadRating(int pos, float rating) {
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.URL_RATING_1 + Constant.arrayList_video.get(pos).getId() +
                    Constant.URL_RATING_2 + "video" + Constant.URL_RATING_3 + String.valueOf(rating) +
                    Constant.URL_RATING_4 + deviceId,String.valueOf(pos),"rating");
        }
    }

    private	class MyTask extends AsyncTask<String, Void, String> {

        String type = "";
        String rate = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String json = JsonUtils.getJSONString(params[0]);
            type = params[2];

            if(type.equals("rating")) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG_ROOT);
                    JSONObject jsonObj = jsonArray.getJSONObject(0);
                    rate = jsonObj.getString("rate_avg");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return params[1];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            int p = Integer.parseInt(result);
            switch (type) {
                case "views":
                    int tot = Integer.parseInt(Constant.arrayList_video.get(p).getTotalViews());
                    Constant.arrayList_video.get(p).setTotalViews("" + (tot + 1));
                    dbHelper.updateViewVideo(Constant.arrayList_video.get(pos).getId(), Constant.arrayList_video.get(p).getTotalViews(),"video");
                    break;
                case "rating":
//                    float rating = Float.parseFloat(Constant.arrayList_video.get(p).getRateAvg());
                    if(!rate.equals("")) {
                        Constant.arrayList_video.get(p).setRateAvg(rate);
                        dbHelper.updateViewVideo(Constant.arrayList_video.get(pos).getId(), Constant.arrayList_video.get(p).getRateAvg(), "rate");
                        Toast.makeText(DetailVideos.this, getResources().getString(R.string.rating_submit), Toast.LENGTH_SHORT).show();
                        rating.setRating(Float.parseFloat(rate));
                    } else {
                        Toast.makeText(DetailVideos.this, getResources().getString(R.string.already_rated), Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                    break;
            }
        }
    }

    public void setFavImage() {
        if(isFav) {
            imageView_fav.setImageResource(R.mipmap.heart_red_round_hover);
        } else {
            imageView_fav.setImageResource(R.mipmap.heart_red_round);
        }
    }

    private void openRateDialog() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new Dialog(DetailVideos.this,android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            dialog = new Dialog(DetailVideos.this);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_rating);

        final RatingBar ratingBar = (RatingBar)dialog.findViewById(R.id.rating_wall);
        ratingBar.setMax(5);
        ratingBar.setNumStars(5);
        AppCompatButton button_submit = (AppCompatButton)dialog.findViewById(R.id.button_submit_rating_wall);

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadRating(pos, ratingBar.getRating());
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void playVideo() {
        switch (Constant.arrayList_video.get(pos).getType()) {
            case "youtube":
                if(isPackageInstalled("com.google.android.youtube",getPackageManager())) {
                    Intent intent = YouTubeStandalonePlayer.createVideoIntent(DetailVideos.this, getResources().getString(R.string.youtube_app_id), Constant.arrayList_video.get(pos).getVideo_id(), 0, true, false);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, getResources().getString(R.string.no_yt_installed), Toast.LENGTH_SHORT).show();
                }
                break;
            case "server_url":
                Intent intent_server = new Intent(DetailVideos.this, PlayServerVideos.class);
                intent_server.putExtra("url",Constant.arrayList_video.get(pos).getUrl());
                intent_server.putExtra("type","server_url");
                startActivity(intent_server);
                break;
            case "local":
                Intent intent_local = new Intent(DetailVideos.this, PlayServerVideos.class);
                intent_local.putExtra("url",Constant.arrayList_video.get(pos).getUrl());
                intent_local.putExtra("type","local");
                startActivity(intent_local);
                break;
            case "dailymotion":
                Intent intent_daily = new Intent(DetailVideos.this, PlayServerVideos.class);
                intent_daily.putExtra("vid",Constant.arrayList_video.get(pos).getVideo_id());
                intent_daily.putExtra("type","dailymotion");
                startActivity(intent_daily);
                break;
            case "vimeo":
                Intent i = new Intent(DetailVideos.this,Vimeo.class);
                i.putExtra("id", Constant.arrayList_video.get(pos).getVideo_id());
                startActivity(i);
//                Intent intent_vimeo = new Intent(DetailVideos.this, PlayServerVideos.class);
//                intent_vimeo.putExtra("vid",Constant.arrayList_video.get(pos).getVideo_id());
//                intent_vimeo.putExtra("type","vimeo");
//                startActivity(intent_vimeo);
                break;
        }
    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private void hideSystemUI(View mDecorView) {
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    // This snippet shows the system bars.
    private void showSystemUI(View mDecorView) {
        mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
//        mDecorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public void showInterAd(final String option) {

        Constant.adCount = Constant.adCount + 1;

        if(Constant.adCount % Constant.adShow == 0) {
            if(mInterstitial.isLoaded()) {
                mInterstitial.show();
                mInterstitial.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        click(option);
                        loadInterAd();
                        super.onAdClosed();
                    }
                });
            } else {
                click(option);
            }
        } else {
            click(option);
        }
    }

    public void loadInterAd() {
        mInterstitial = new InterstitialAd(DetailVideos.this);
        mInterstitial.setAdUnitId(getResources().getString(R.string.admob_intertestial_id));
        mInterstitial.loadAd(new AdRequest.Builder().build());
    }

    private void click(String option) {
        switch (option) {
            case "play":
                playVideo();
                break;
            case "share":
                methods.shareVideo(pos,Constant.arrayList_video.get(pos).getImageBig(),Constant.arrayList_video.get(pos).getTitle(),Constant.arrayList_video.get(pos).getUrl());
                break;
            case "rate":
                openRateDialog();
                break;
            default:
                break;
        }
    }

//    @Override
//    protected void onDestroy() {
//        try {
//            if (Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("youtube")) {
//                youtube.pauseVideo();
//                youtube.release();
//            } else if (Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("dailymotion")) {
//                player_dailymotion.stopLoading();
//                player_dailymotion.pause();
//                player_dailymotion.release();
//            } else if (Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("server_url") || Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("local")) {
//                player_server.pause();
//                player_server.stop();
//                player_server.release();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onPause() {
//        try {
//            if (Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("youtube")) {
//                youtube.pauseVideo();
//            } else if (Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("dailymotion")) {
//                player_dailymotion.pause();
//            } else if (Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("server_url") || Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("local")) {
//                player_server.pause();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        super.onPause();
//    }
//
//    @Override
//    protected void onResume() {
//        try {
//            if(Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("youtube")) {
//                youtube.playVideo();
//            } else if(Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("dailymotion")) {
//                player_dailymotion.play();
//            } else if(Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("server_url") || Constant.arrayList_video.get(viewPager.getCurrentItem()).getType().equals("local")) {
//                player_server.start();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        super.onResume();
//    }
}
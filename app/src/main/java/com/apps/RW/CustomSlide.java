package com.apps.RW;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import agency.tango.materialintroscreen.SlideFragment;

public class CustomSlide extends SlideFragment {

    TextView textView_title, textView_desc;
    ImageView imageView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_custom_slide, container, false);

        textView_title = (TextView)view.findViewById(R.id.tv_intro_title);
        textView_desc = (TextView)view.findViewById(R.id.tv_intro_desc);
        imageView = (ImageView) view.findViewById(R.id.iv_intro);

        Picasso.with(getActivity())
                .load(R.drawable.intro_wall)
                .into(imageView);

        textView_title.setText(getActivity().getResources().getString(R.string.amazing_wall));
        textView_desc.setText(getActivity().getResources().getString(R.string.get_amazing_wall));

        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.black_30;
    }

    @Override
    public int buttonsColor() {
        return R.color.colorAccent;
    }

    @Override
    public boolean canMoveFurther() {
        return true;
    }
}
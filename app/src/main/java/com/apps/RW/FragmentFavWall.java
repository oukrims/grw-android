package com.apps.RW;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.adapter.AdapterLatestWallpaper;
import com.apps.items.ItemWallpaper;
import com.apps.utils.DBHelper;
import com.apps.utils.Methods;

import java.util.ArrayList;

public class FragmentFavWall extends Fragment {

    Methods methods;
    RecyclerView recyclerView;
    GridLayoutManager layout;
    AdapterLatestWallpaper adapterLatestWallpaper;
    ArrayList<ItemWallpaper> arrayList_wallpaper;
    ProgressDialog progressDialog;
    DBHelper dbHelper;
    TextView textView_empty;
    SearchView searchView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_latest_wall,container,false);

        methods = new Methods(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        dbHelper = new DBHelper(getActivity());

        textView_empty = (TextView)v.findViewById(R.id.tv_latest_wall);

        arrayList_wallpaper = new ArrayList<>();

        layout = new GridLayoutManager(getActivity(),2);
        recyclerView = (RecyclerView)v.findViewById(R.id.rv_latest_wall);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layout);

//        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                Constant.arrayList_wall.clear();
//                Constant.arrayList_wall.addAll(arrayList_wallpaper);
//                Intent intent = new Intent(getActivity(),DetailWallpaper.class);
//                int realpos = getPosition(adapterLatestWallpaper.getID(position));
//                intent.putExtra("pos",realpos);
//                startActivity(intent);
//            }
//        }));

        loadDataFromDB();

        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            if (searchView.isIconified()) {
                recyclerView.setAdapter(adapterLatestWallpaper);
                adapterLatestWallpaper.notifyDataSetChanged();
            } else {
                adapterLatestWallpaper.getFilter().filter(s);
                adapterLatestWallpaper.notifyDataSetChanged();
            }
            return true;
        }
    };

    private void loadDataFromDB() {
        arrayList_wallpaper = dbHelper.getFavWall();
        adapterLatestWallpaper = new AdapterLatestWallpaper(getActivity(),arrayList_wallpaper);
        recyclerView.setAdapter(adapterLatestWallpaper);

        if(arrayList_wallpaper.size() == 0) {
            textView_empty.setVisibility(View.VISIBLE);
        } else {
            textView_empty.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        if(adapterLatestWallpaper != null && arrayList_wallpaper.size()>0) {
            adapterLatestWallpaper.notifyDataSetChanged();
        }
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser) {
            if(adapterLatestWallpaper != null && arrayList_wallpaper.size()>0) {
                adapterLatestWallpaper.notifyDataSetChanged();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }
}
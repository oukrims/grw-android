package com.apps.RW;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.apps.utils.Methods;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class VideosActivity extends AppCompatActivity {

    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    Methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);

        methods = new Methods(this);

        AdView mAdView = (AdView) findViewById(R.id.adView_video);
        mAdView.loadAd(new AdRequest.Builder().build());

        toolbar = (Toolbar)findViewById(R.id.toolbar_videos);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.videos));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        methods.setStatusColor(getWindow(),toolbar);
        methods.forceRTLIfSupported(getWindow());

        viewPager = (ViewPager)findViewById(R.id.vp_videos);
        tabLayout = (TabLayout)findViewById(R.id.tabs_videos);
        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(3);

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new FragmentLatestVideo().newInstance(position);
                case 1:
                    return new FragmentCategoryVideo();
                case 2:
                    return new FragmentLatestVideo().newInstance(position);
//                case 3:
//                    return new Fragment360Videos();
                case 3:
//                    return new FragmentWallpapers();
                default:
                    return new FragmentLatestVideo();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.latest);

                case 1:
                    return getResources().getString(R.string.popular);
//
                default:
                    return getResources().getString(R.string.latest);
            }
        }
    }
}

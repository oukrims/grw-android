package com.apps.RW;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.items.UserItem;
import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    Button button_login, button_skip;
    AppCompatEditText editText_email, editText_password;
    AppCompatCheckBox checkBox_rememberme;
    TextView textView_forgotpass, textView_signup;
    ProgressDialog progressDialog;
    Methods methods;
    UserLoginTask userLoginTask;

    private String from = "";
    private Dialog dialog;
    private Boolean isRemember = false;
    String username="",password="";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        methods = new Methods(this);
        methods.forceRTLIfSupported(getWindow());
        methods.setStatusColor(getWindow(),null);

        sharedPreferences = getSharedPreferences("login",MODE_PRIVATE);
        editor = sharedPreferences.edit();

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);

        button_login = (Button)findViewById(R.id.button_login);
        button_skip = (Button)findViewById(R.id.button_skip);
        editText_email = (AppCompatEditText)findViewById(R.id.et_email);
        editText_password = (AppCompatEditText)findViewById(R.id.et_password);
        checkBox_rememberme = (AppCompatCheckBox) findViewById(R.id.checkBox);
        textView_forgotpass = (TextView) findViewById(R.id.tv_forgotpass);
        textView_signup = (TextView) findViewById(R.id.tv_signup);

        textView_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        editText_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        button_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
        });

        textView_forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openForgetPassDialog();
            }
        });

        checkBox_rememberme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkBox_rememberme.isChecked()) {
                    isRemember = true;
                } else {
                    isRemember = false;
                }
            }
        });
    }

    private void openForgetPassDialog() {
        dialog = new Dialog(LoginActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_forgetpass);

        final AppCompatEditText editText = (AppCompatEditText)dialog.findViewById(R.id.editText_forgetpass);
        Button button = (Button)dialog.findViewById(R.id.button_forgetpass);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().trim().isEmpty()) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_empty), Toast.LENGTH_SHORT).show();
                } else {
                    new LoadForgetPass().execute(Constant.URL_FORGOT_PASS + editText.getText().toString());
                }
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    class LoadForgetPass extends AsyncTask<String, String, String> {

        String suc="";
        String msg="";

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        protected String doInBackground(String... args) {

            String json_strng = JsonUtils.getJSONString(args[0]);
            try {
                JSONObject json = new JSONObject(json_strng);
                JSONArray c = json.getJSONArray(Constant.TAG_ROOT);

                JSONObject object = c.getJSONObject(0);
                suc = object.getString(Constant.TAG_SUCCESS);
                msg = object.getString("msg");

                return "1";
            } catch (JSONException e) {
                e.printStackTrace();
                return "0";
            } catch (Exception e) {
                e.printStackTrace();
                return "0";
            }
        }

        protected void onPostExecute(String success) {
            progressDialog.dismiss();
            if(success.equals("1")) {
                if(suc.equals("1")) {

                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_newpass), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_notfound), Toast.LENGTH_SHORT).show();
                }
            } else if(success.equals("0")){
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.server_no_conn), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void attemptLogin() {
        editText_email.setError(null);
        editText_password.setError(null);

        // Store values at the time of the login attempt.
        String email = editText_email.getText().toString();
        String password = editText_password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            editText_password.setError(getString(R.string.error_password_sort));
            focusView = editText_password;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            editText_email.setError(getString(R.string.cannot_empty));
            focusView = editText_email;
            cancel = true;
        } else if (!isEmailValid(email)) {
            editText_email.setError(getString(R.string.error_invalid_email));
            focusView = editText_email;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
//            showProgress(true);
            userLoginTask = new UserLoginTask(Constant.URL_LOGIN_1+email+Constant.URL_LOGIN_2+password);
            userLoginTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 0;
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String url;
        private String suc = "";
        private String msg = "";

        UserLoginTask(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            String json = JsonUtils.getJSONString(url);
            try {
                JSONObject jsonObject = new JSONObject(json);
                JSONArray jsonArray = jsonObject.getJSONArray(Constant.TAG_ROOT);

                JSONObject object = jsonArray.getJSONObject(0);
                suc = object.getString(Constant.TAG_SUCCESS);
                if(suc.equals("0")) {
                    msg = object.getString(Constant.TAG_MSG);
                } else {
                    Constant.user_id = object.getString(Constant.TAG_USER_ID);
                    Constant.userItem = new UserItem(Constant.user_id,object.getString(Constant.TAG_NAME),"","","","");
                }

                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            } catch (Exception ee) {
                ee.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            userLoginTask = null;
            progressDialog.dismiss();

            if (success) {
                if(suc.equals("0")) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.email_pass_nomatch),Toast.LENGTH_SHORT).show();
                } else {
                    if(isRemember) {
                        username = editText_email.getText().toString();
                        password = editText_password.getText().toString();
                        editor.putBoolean("rem", true);
                        editor.putString("user",username);
                        editor.putString("pass",password);
                        editor.putString("uid",Constant.user_id);
                        editor.commit();
                    }
                    Constant.isLogged = true;
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                    if(from.equals("app")) {
                        Constant.isLoginChanged = true;
                        finish();
                    } else {
                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                }

                    finish();
                }
            } else {
                Toast.makeText(LoginActivity.this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            userLoginTask = null;
        }
    }
}

package com.apps.RW;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Toast;

import com.apps.adapter.AdapterNav;
import com.apps.items.ItemAbout;
import com.apps.utils.Constant;
import com.apps.utils.DBHelper;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;
import com.apps.utils.RecyclerItemClickListener;
import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.morsebyte.shailesh.twostagerating.FeedbackReceivedListener;
import com.morsebyte.shailesh.twostagerating.FeedbackWithRatingReceivedListener;
import com.morsebyte.shailesh.twostagerating.TwoStageRate;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    FragmentManager fm;
    RecyclerView recyclerView;
    AdapterNav adapterNav;
    Methods methods;
    GridLayoutManager lLayout;
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 101;
    DBHelper dbHelper;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int id = 1;
    private static Context context;
    private ConsentForm form;
    private AdRequest request;
    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);



        ConsentInformation consentInformation = ConsentInformation.getInstance(MainActivity.this);
        String[] publisherIds = {"pub-8433520436694951"};
        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                // User's consent status successfully updated.

                if(!ConsentInformation.getInstance(getApplicationContext()).isRequestLocationInEeaOrUnknown()){
                    if(consentStatus == ConsentStatus.PERSONALIZED || consentStatus == ConsentStatus.NON_PERSONALIZED){

                        Bundle extras = new Bundle();
                        extras.putString("npa", "1");

                        request = new AdRequest.Builder()
                                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                                .build();

                    }else{
                        URL privacyUrl = null;
                        try {
                            // TODO: Replace with your app's privacy policy URL.
                            privacyUrl = new URL("https://support.google.com/admob/answer/7666366?hl=en");
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            // Handle error.
                        }
                        form = new ConsentForm.Builder(getApplicationContext(), privacyUrl)
                                .withListener(new ConsentFormListener() {
                                    @Override
                                    public void onConsentFormLoaded() {
                                        // Consent form loaded successfully.
                                    }

                                    @Override
                                    public void onConsentFormOpened() {
                                        // Consent form was displayed.
                                    }

                                    @Override
                                    public void onConsentFormClosed(
                                            ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                                        // Consent form was closed.
                                    }

                                    @Override
                                    public void onConsentFormError(String errorDescription) {
                                        // Consent form error.
                                    }
                                })
                                .withPersonalizedAdsOption()
                                .withNonPersonalizedAdsOption()
                                .withAdFreeOption()
                                .build();
                        form.load();
                        form.show();
                    }
                }
            }



            @Override
            public void onFailedToUpdateConsentInfo(String errorDescription) {
                // User's consent status failed to update.
            }
        });



        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler() )
                .init();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "rw");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());

        methods = new Methods(this);
        methods.setStatusColor(getWindow(),toolbar);
        methods.forceRTLIfSupported(getWindow());

        TwoStageRate twoStageRate = TwoStageRate.with(this);
        //Setting conditions
        twoStageRate.setInstallDays(1).setEventsTimes(1).setLaunchTimes(1);
        twoStageRate.resetOnDismiss(true).resetOnFeedBackDeclined(true).resetOnRatingDeclined(false);
        twoStageRate.showIfMeetsConditions();

        //Setting feedback listener
        twoStageRate.setFeedbackReceivedListener(new FeedbackReceivedListener() {
            @Override
            public void onFeedbackReceived(String feedback) {
                Toast.makeText(MainActivity.this, feedback, Toast.LENGTH_SHORT).show();
            }
        });

        //Setting texts for initial prompt
        twoStageRate.with(this).setRatePromptTitle("How did you find this app").
                setRatePromptLaterText("LATER_TEXT").setRatePromptNeverText("NEVER_TEXT").setRatePromptDismissible(false);

//Setting texts for confirmation dialog
        twoStageRate.with(this).setConfirmRateDialogTitle("CONFIRMATION_TITLE").
                setConfirmRateDialogDescription("CONFIRMATION_DESCRITPION").
                setConfirmRateDialogPositiveText("POSITIVE_BUTTON_TEXT").
                setConfirmRateDialogNegativeText("NEGATIVE_BUTTON_TEXT").
                setConfirmRateDialogDismissible(true);

        //Setting texts for feedback title
        twoStageRate.with(this).setFeedbackDialogTitle("FEEDBACK_TITLE").
                setFeedbackDialogDescription("FEEDBACK_DIALOG_DESCRIPTION").
                setFeedbackDialogPositiveText("POSITIVE_BUTTON_TEXT").
                setFeedbackDialogNegativeText("NEGATIVE_BUTTON_TEXT");

        fm = getSupportFragmentManager();
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        lLayout = new GridLayoutManager(this, 3);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        recyclerView = (RecyclerView)navigationView.findViewById(R.id.rv_nav);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

        adapterNav = new AdapterNav(this);
        recyclerView.setAdapter(adapterNav);

        FragmentHome f1 = new FragmentHome();
        loadFrag(f1,getResources().getString(R.string.app_name),fm);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(MainActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (position) {
                    case 0:
                        FragmentHome f1 = new FragmentHome();
                        loadFrag(f1,getResources().getString(R.string.app_name),fm);
                        break;
                    case 1:
                        Intent intent_wall = new Intent(MainActivity.this,WallpaperActivity.class);
                        startActivity(intent_wall);
                        break;
                    case 2:
                        Intent intent_ring = new Intent(MainActivity.this,RingtoneActivity.class);
                        startActivity(intent_ring);
                        break;
                    case 3:
                        Intent intent_gif = new Intent(MainActivity.this,ByCatWallActivity.class);
                        intent_gif.putExtra("cid","0");
                        intent_gif.putExtra("cname","gifs");
                        startActivity(intent_gif);
                        break;
                    case 4:
                        Intent intent_fav = new Intent(MainActivity.this,FavouriteActivity.class);
                        startActivity(intent_fav);
                        break;

                    case 5:
                        Intent intent_upload = new Intent(MainActivity.this,UploadActivity.class);
                        startActivity(intent_upload);
                        break;
                    case 6:
                        FragmentProfile fprof = new FragmentProfile();
                        loadFrag(fprof,getResources().getString(R.string.profile),fm);
                        break;
                    case 7:
                        FragmentFeedBack ffb = new FragmentFeedBack();
                        loadFrag(ffb,getResources().getString(R.string.feedback),fm);
                        break;
                    case 8:
                        Intent intent_abt = new Intent(MainActivity.this,AboutActivity.class);
                        startActivity(intent_abt);
                        break;
                    case 9:
                        moreapp();
                        break;
                    case 10:
                        shareapp();
                        break;
                    case 11:
                        rateapp();
                        break;
                    case 12:
                        openPrivacyDialog();
                        break;
                    case 13:
                        methods.clickLogin();
                        break;
                }

                drawer.closeDrawer(GravityCompat.START);
            }
        }));

        if (methods.isNetworkAvailable()) {
            new MyTask().execute(Constant.APP_DETAILS_URL);
        }

//        FragmentHome f1 = new FragmentHome();
//        loadFrag(f1,"home",fm);
//        getSupportActionBar().setTitle(getResources().getString(R.string.home));

        checkPer();
    }

    private void displayPrivacyForm() {
        URL privacyUrl = null;
        try {
            // TODO: Replace with your app's privacy policy URL.
            privacyUrl = new URL("https://www.your.com/privacyurl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            // Handle error.
        }
        form = new ConsentForm.Builder(context, privacyUrl)
                .withListener(new ConsentFormListener() {
                    @Override
                    public void onConsentFormLoaded() {
                        // Consent form loaded successfully.
                    }

                    @Override
                    public void onConsentFormOpened() {
                        // Consent form was displayed.
                    }

                    @Override
                    public void onConsentFormClosed(
                            ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                        // Consent form was closed.
                    }

                    @Override
                    public void onConsentFormError(String errorDescription) {
                        // Consent form error.
                    }
                })
                .withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .withAdFreeOption()
                .build();
        form.load();
        form.show();
    }

    public void loadFrag(Fragment f1, String name, FragmentManager fm) {

        FragmentTransaction ft = fm.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.frame_nav, f1, name);
        ft.commit();

        getSupportActionBar().setTitle(name);
    }

    public void rateapp() {

        Toast.makeText(MainActivity.this, "Tap to hide", Toast.LENGTH_SHORT).show();


        TwoStageRate twoStageRate = TwoStageRate.with(this);
//initialises condintions to 5 days of use, 10 times of launch use or 5 triggers of custom event.
        twoStageRate.setInstallDays(1).setLaunchTimes(1).setEventsTimes(0);

//If user dismisses it, it simply resets again. (when user dismissed by clicking anywhere else on screen)
        twoStageRate.resetOnDismiss(true);  //it is true by default

//If user gives rating the first time but declines to give playstore rating/ feedback we can reset the
//TwoStageRate. These are false by default.
        twoStageRate.resetOnFeedBackDeclined(true).resetOnRatingDeclined(true);

//You may choose to show/hide your app icon in rating prompt (default true)
        //Feedback listener giving back only the feedback
        twoStageRate.setFeedbackReceivedListener(new FeedbackReceivedListener() {
            @Override
            public void onFeedbackReceived(String feedback) {
                Toast.makeText(MainActivity.this, feedback, Toast.LENGTH_SHORT).show();
            }
        });

        //Feedback listener with rating information as well
        twoStageRate.setFeedbackWithRatingReceivedListener(new FeedbackWithRatingReceivedListener() {
            @Override
            public void onFeedbackReceived(float rating, String feedback) {
                Toast.makeText(MainActivity.this, "Rating :" + rating + "Feedback :" + feedback, Toast.LENGTH_SHORT).show();
                //change the playstore link wa hna !!!!!!!!!!!!!
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=com.wallpapers.glitter")));
            }
        });

//Finally call to show feedback dialog if any of condition is met.
        twoStageRate.showIfMeetsConditions();
    }

    public void shareapp() {
        Intent ishare = new Intent(Intent.ACTION_SEND);
        ishare.setType("text/plain");
        ishare.putExtra(Intent.EXTRA_TEXT,getResources().getString(R.string.app_name)+" - http://play.google.com/store/apps/details?id="+getPackageName());
        startActivity(ishare);
    }

    public void moreapp() {
      //  startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(getString(R.string.play_more_apps))));
        Intent intent_ring = new Intent(MainActivity.this,More_apps.class);
        startActivity(intent_ring);
    }

    public void openPrivacyDialog() {
        Dialog dialog;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new Dialog(MainActivity.this,android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            dialog = new Dialog(MainActivity.this);
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_privacy);

        WebView webview = (WebView)dialog.findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
	//webview.loadUrl("http://localhost/wr/apps.php");
        String mimeType = "text/html;charset=UTF-8";
        String encoding = "utf-8";

        if(Constant.itemAbout != null) {
            String text = "<html><head>"
                    + "<style> body{color: #000 !important;text-align:left}"
                    + "</style></head>"
                    + "<body>"
                    + Constant.itemAbout.getPrivacy()
                    + "</body></html>";

            webview.loadData(text, mimeType, encoding);
        }

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }


    private	class MyTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            pbar.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

//            pbar.dismiss();

            if (null == result || result.length() == 0) {
                Toast.makeText(MainActivity.this, getString(R.string.no_data_found), Toast.LENGTH_SHORT).show();

            } else {

                try {
                    JSONObject mainJson = new JSONObject(result);
                    JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);
                    JSONObject  c = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        c = jsonArray.getJSONObject(i);

                        String appname = c.getString("app_name");
                        String applogo = c.getString("app_logo");
                        String desc = c.getString("app_description");
                        String appversion = c.getString("app_version");
                        String appauthor = c.getString("app_author");
                        String appcontact = c.getString("app_contact");
                        String email = c.getString("app_email");
                        String website = c.getString("app_website");
                        String privacy = c.getString("app_privacy_policy");
                        String developedby = c.getString("app_developed_by");

                        Constant.itemAbout = new ItemAbout(appname,applogo,desc,appversion,appauthor,appcontact,email,website,privacy,developedby);
//						dbHelper.addtoAbout();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void checkPer()
    {
        if ((ContextCompat.checkSelfPermission(MainActivity.this,"android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE","android.permission.READ_PHONE_STATE"},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        boolean canUseExternalStorage = false;

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    canUseExternalStorage = true;
                }

                if (!canUseExternalStorage) {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.cannot_use_save_permission), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Toast.makeText(MainActivity.this, "Tap to hide", Toast.LENGTH_SHORT).show();

            AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.AppTheme_PopupOverlay));
            alert.setTitle(getString(R.string.app_name));
            alert.setIcon(R.drawable.logo_login);
            alert.setMessage(getResources().getString(R.string.sure_quit));

            alert.setPositiveButton(getResources().getString(R.string.yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                            finish();
                        }
                    });
            alert.show();
            TwoStageRate twoStageRate = TwoStageRate.with(this);
//initialises condintions to 5 days of use, 10 times of launch use or 5 triggers of custom event.
            twoStageRate.setInstallDays(1).setLaunchTimes(1).setEventsTimes(0);

//If user dismisses it, it simply resets again. (when user dismissed by clicking anywhere else on screen)
            twoStageRate.resetOnDismiss(true);  //it is true by default

//If user gives rating the first time but declines to give playstore rating/ feedback we can reset the
//TwoStageRate. These are false by default.
            twoStageRate.resetOnFeedBackDeclined(true).resetOnRatingDeclined(true);



//You may choose to show/hide your app icon in rating prompt (default true)
            //Feedback listener giving back only the feedback
            twoStageRate.setFeedbackReceivedListener(new FeedbackReceivedListener() {
                @Override
                public void onFeedbackReceived(String feedback) {
                    Toast.makeText(MainActivity.this, feedback, Toast.LENGTH_SHORT).show();
                }
            });

            //Feedback listener with rating information as well
            twoStageRate.setFeedbackWithRatingReceivedListener(new FeedbackWithRatingReceivedListener() {
                @Override
                public void onFeedbackReceived(float rating, String feedback) {
                    Toast.makeText(MainActivity.this, "Rating :" + rating + "Feedback :" + feedback, Toast.LENGTH_SHORT).show();
                    //change the app playstore link

                    startActivity(new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=com.wallpapers.glitter")));
                }
            });

//Finally call to show feedback dialog if any of condition is met.
            twoStageRate.showIfMeetsConditions();




        }
        return super.onKeyDown(keyCode, event);

    }
}

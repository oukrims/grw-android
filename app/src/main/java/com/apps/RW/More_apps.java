package com.apps.RW;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.Methods;


public class More_apps extends AppCompatActivity {
    private WebView webview;
    int pos;
    Toolbar toolbar;
    Methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_apps);


        methods = new Methods(this);
        toolbar = (Toolbar)findViewById(R.id.toolbar_more_apps);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.apps));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        methods.setStatusColor(getWindow(),toolbar);
        methods.forceRTLIfSupported(getWindow());
        if(methods.isNetworkAvailable()) {


        webview =(WebView)findViewById(R.id.more_apps);

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Uri.parse(url).getScheme().equals("market")) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(url));
                        Activity host = (Activity) view.getContext();
                        host.startActivity(intent);
                        return true;
                    } catch (ActivityNotFoundException e) {
                        // Google Play app is not installed, you may want to open the app store link
                        Uri uri = Uri.parse(url);
                        view.loadUrl("http://play.google.com/store/apps/" + uri.getHost() + "?" + uri.getQuery());
                        return false;
                    }

                }
                return false;
            }
        });
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
            webview.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
            webview.loadUrl(Constant.SERVER_URL+"/apps.php");
        } else {
            Toast.makeText(More_apps.this, getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
        }


        //toolabr back arrow
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed(); // calls the System onBackPressed method
            }

        });
    }
}

package com.apps.RW;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.adapter.AdapterLatestVideo;
import com.apps.items.ItemVideos;
import com.apps.utils.DBHelper;
import com.apps.utils.Methods;

import java.util.ArrayList;

public class FragmentFavVideo extends Fragment {

    Methods methods;
    RecyclerView recyclerView;
    LinearLayoutManager layout;
    AdapterLatestVideo adapterLatestVideo;
    ArrayList<ItemVideos> arrayList_videos;
    DBHelper dbHelper;
    TextView textView_empty;
    SearchView searchView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_latest_wall,container,false);

        methods = new Methods(getActivity());

        dbHelper = new DBHelper(getActivity());

        textView_empty = (TextView)v.findViewById(R.id.tv_latest_wall);

        arrayList_videos = new ArrayList<>();

        layout = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView)v.findViewById(R.id.rv_latest_wall);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layout);

//        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                Constant.arrayList_video.clear();
//                Constant.arrayList_video.addAll(arrayList_videos);
//                Intent intent = new Intent(getActivity(),DetailVideos.class);
//                int realpos = getPosition(adapterLatestVideo.getID(position));
//                intent.putExtra("pos",realpos);
//                startActivity(intent);
//            }
//        }));

        loadDataFromDB();

        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            if (searchView.isIconified()) {
                recyclerView.setAdapter(adapterLatestVideo);
                adapterLatestVideo.notifyDataSetChanged();
            } else {
                adapterLatestVideo.getFilter().filter(s);
                adapterLatestVideo.notifyDataSetChanged();
            }
            return true;
        }
    };

    private void loadDataFromDB() {
        arrayList_videos = dbHelper.getFavVideo();
        adapterLatestVideo = new AdapterLatestVideo(getActivity(),arrayList_videos);
        recyclerView.setAdapter(adapterLatestVideo);

        if(arrayList_videos.size() == 0) {
            textView_empty.setVisibility(View.VISIBLE);
        } else {
            textView_empty.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        if(adapterLatestVideo != null && arrayList_videos.size()>0) {
            adapterLatestVideo.notifyDataSetChanged();
        }
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if(isVisibleToUser) {
            if(adapterLatestVideo != null && arrayList_videos.size()>0) {
                adapterLatestVideo.notifyDataSetChanged();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }
}
package com.apps.RW;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.apps.utils.Constant;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class FragmentUploadWall extends Fragment {

    Methods methods;
    ProgressDialog progressDialog, progress;
    Spinner spinner;
    EditText editText;
    AppCompatButton button_browse, button_submit;
    ImageView imageView;
    ArrayList<String> arrayList_cat;
    ArrayList<String> arrayList_catid;
    private int PICK_IMAGE_REQUEST = 1;
    Bitmap bitmap_upload;
    String msg = "", suc = "", imagePath="", cat_id;
    HttpURLConnection conn;
    private int serverResponseCode = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upload_wall, container, false);

        methods = new Methods(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        progress = new ProgressDialog(getActivity());
        progress.setMessage(getActivity().getResources().getString(R.string.loading));
        progress.setCancelable(false);

        button_browse = (AppCompatButton) v.findViewById(R.id.button_upload_wall_browse);
        button_submit = (AppCompatButton) v.findViewById(R.id.button_upload_wall_submit);

        imageView = (ImageView) v.findViewById(R.id.iv_upload_wall_submit);

        arrayList_cat = new ArrayList<>();
        arrayList_catid = new ArrayList<>();

        editText = (EditText) v.findViewById(R.id.et_upload_wall);
        spinner = (Spinner) v.findViewById(R.id.spinner_upload_wallcat);

            if (methods.isNetworkAvailable()) {
                new LoadCat().execute();
            } else {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
            }

            button_browse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                }
            });

            button_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Constant.isLogged) {
                        if(methods.isNetworkAvailable()) {
                            if(imagePath.equals("")) {
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.select_wallpaper), Toast.LENGTH_SHORT).show();
                            } else {
                                new UploadImage().execute();
                            }
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        methods.clickLogin();
                    }
                }
            });

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    cat_id = arrayList_catid.get(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        return v;
    }

    private class LoadCat extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            progress.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                String json = JsonUtils.getJSONString(Constant.URL_CAT_WALL);
                JSONObject mainJson = new JSONObject(json);
                JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject format = jsonArray.getJSONObject(i);
                    String name = format.getString(Constant.TAG_CAT_NAME);
                    String id = format.getString(Constant.TAG_CAT_ID);

                    arrayList_cat.add(name);
                    arrayList_catid.add(id);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progress.isShowing()) {
                progress.dismiss();
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner, arrayList_cat);
            spinner.setAdapter(adapter);

            if(arrayList_catid.size() > 0) {
                cat_id = arrayList_catid.get(0);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            imagePath = methods.getPathImage(uri);

            try {
                bitmap_upload = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                imageView.setImageBitmap(bitmap_upload);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
//        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
//        cursor.moveToFirst();
//        String document_id = cursor.getString(0);
//        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
//        cursor.close();
//
//        cursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
//        cursor.moveToFirst();
//        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
//        cursor.close();
//
//        return path;

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                String filePath = "";
                String wholeID = DocumentsContract.getDocumentId(uri);

                // Split at colon, use second item in the array
                String id = wholeID.split(":")[1];

                String[] column = {MediaStore.Images.Media.DATA};

                // where id is equal to
                String sel = MediaStore.Images.Media._ID + "=?";

                Cursor cursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);

                int columnIndex = cursor.getColumnIndex(column[0]);

                if (cursor.moveToFirst()) {
                    filePath = cursor.getString(columnIndex);
                }
                cursor.close();
                return filePath;
            } else {

                if (uri == null) {
                    return null;
                }
                // try to retrieve the image from the media store first
                // this will only work for images selected from gallery
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
                if (cursor != null) {
                    int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    return cursor.getString(column_index);
                }
                // this is our fallback here
                return uri.getPath();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (uri == null) {
                return null;
            }
            // try to retrieve the image from the media store first
            // this will only work for images selected from gallery
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            // this is our fallback here
            return uri.getPath();
        }

    }

    public int uploadFile(String sourceFileUri) {
        String tags = editText.getText().toString();

        final String upLoadServerUri = Constant.URL_UPLOAD_WALL;

        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        String as[] = null;

        as = new String[1];
        as[0] = sourceFile.toString();


        if (!sourceFile.isFile()) {
            Log.e("uploadFile", getResources().getString(R.string.source_file_not_found));
            progressDialog.dismiss();
            return 0;
        }
        try { // open a URL connection to the Servlet

            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(upLoadServerUri);
            conn = (HttpURLConnection) url.openConnection(); // Open a HTTP  connection to  the URL
//            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            //*********filename first is php base filename
            conn.setRequestProperty("wallpaper_image", sourceFile.getAbsolutePath());

            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            //********filename first is php base filename
            dos.writeBytes("Content-Disposition: form-data; name=\"wallpaper_image\";filename=\"" + sourceFile.getAbsolutePath() + "\"" + lineEnd);
            dos.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available(); // create a buffer of  maximum size

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necesssary after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + '\n');
            }
            String jsonString = stringBuilder.toString();
            Log.e("json", jsonString);
            JSONObject jsonObj = new JSONObject(jsonString);

            try {
                JSONArray jsonArray = jsonObj.getJSONArray(Constant.TAG_ROOT);
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                String wall_name = jsonObject.getString("wallpaper_image_name");
                String success = jsonObject.getString("success");

                if (success.equals("1")) {
                    List<NameValuePair> data2 = new ArrayList<NameValuePair>();
                    JSONObject jObj = JsonUtils.makeHttpRequest(Constant.URL_UPLOAD_WALL_RESP_1 + cat_id + Constant.URL_UPLOAD_WALL_RESP_2 + Constant.user_id + Constant.URL_UPLOAD_WALL_RESP_3 + Constant.userItem.getName() + Constant.URL_UPLOAD_WALL_RESP_4 + wall_name + Constant.URL_UPLOAD_WALL_RESP_5 + tags, "GET", data2);
                    JSONArray jsonArr = jObj.getJSONArray(Constant.TAG_ROOT);
                    JSONObject Obj = jsonArr.getJSONObject(0);

//                    String meessage = Obj.getString("msg");
                    suc = Obj.getString("success");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            Toast.makeText(getActivity(), "Exception : " + e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("Upload file to server", "Exception : " + e.getMessage(), e);
        }
        progressDialog.dismiss();
        return serverResponseCode;
    }

    private class UploadImage extends AsyncTask<Bitmap, Void, String> {

        ProgressDialog loading;
        String tags;

        @Override
        protected void onPreExecute() {
            try {
                super.onPreExecute();
                tags = editText.getText().toString();
                loading = ProgressDialog.show(getActivity(), getResources().getString(R.string.upload_image), "Please wait...", true, true);
                loading.show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Bitmap... params) {
            try {
                uploadFile(imagePath);
                return "";
            }catch(Exception e){
                e.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
            if(suc.equals("1")) {
                Toast.makeText(getActivity(), getResources().getString(R.string.wall_upload_success), Toast.LENGTH_LONG).show();
                editText.setText("");
                imageView.setImageBitmap(null);
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.error_uploading), Toast.LENGTH_LONG).show();
            }
        }
    }
}
package com.apps.RW;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.apps.utils.Methods;
import com.dailymotion.android.player.sdk.PlayerWebView;

public class PlayServerVideos extends AppCompatActivity {

    EasyVideoPlayer player;
    PlayerWebView player_dailymotion;
    String url = "", type = "", video_id_daily;
    Methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_server_videos);

        methods = new Methods(this);
        methods.setStatusColor(getWindow(),null);
        methods.forceRTLIfSupported(getWindow());

        registerReceiver(onCallIncome, new IntentFilter("android.intent.action.PHONE_STATE"));

        type = getIntent().getStringExtra("type");
        if(type.equals("dailymotion")) {
            video_id_daily = getIntent().getStringExtra("vid");
        } else {
            url = getIntent().getStringExtra("url");
        }
        player_dailymotion = (PlayerWebView) findViewById(R.id.player_daily);
        player = (EasyVideoPlayer) findViewById(R.id.player);

        if(type.equals("server_url") || type.equals("local")) {
            player_dailymotion.setVisibility(View.GONE);
            player.setSource(Uri.parse(url));
            player.setAutoPlay(true);
            player.setCallback(easyVideoCallback);
            player.setAutoFullscreen(true);
        } else if(type.equals("dailymotion")) {
            player.setVisibility(View.GONE);
            player_dailymotion.load(video_id_daily);
        }
    }

    EasyVideoCallback easyVideoCallback = new EasyVideoCallback() {
        @Override
        public void onStarted(EasyVideoPlayer player) {

        }

        @Override
        public void onPaused(EasyVideoPlayer player) {
            player.pause();
        }

        @Override
        public void onPreparing(EasyVideoPlayer player) {

        }

        @Override
        public void onPrepared(EasyVideoPlayer player) {
            player.start();
        }

        @Override
        public void onBuffering(int percent) {

        }

        @Override
        public void onError(EasyVideoPlayer player, Exception e) {
            Toast.makeText(PlayServerVideos.this, getResources().getString(R.string.server_no_conn), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCompletion(EasyVideoPlayer player) {

        }

        @Override
        public void onRetry(EasyVideoPlayer player, Uri source) {

        }

        @Override
        public void onSubmit(EasyVideoPlayer player, Uri source) {

        }
    };

    private void playPause(Boolean isPlay) {
        if(type.equals("dailymotion")) {
            if(isPlay) {
                player_dailymotion.play();
            } else {
                player_dailymotion.pause();
            }
        } else if(type.equals("server_url") || type.equals("server_url")) {
            if(isPlay) {
                player.start();
            } else {
                player.pause();
            }
        }
    }

    void hideStatusBar() {
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    BroadcastReceiver onCallIncome = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

            if(a.equals(TelephonyManager.EXTRA_STATE_RINGING) || a.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                playPause(false);
            } else if (a.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                playPause(true);
            }

        }
    };

    @Override
    protected void onPause() {
        playPause(false);
        super.onPause();
    }

    @Override
    protected void onResume() {
        hideStatusBar();
        playPause(true);
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(onCallIncome);
        super.onDestroy();
    }
}

package com.apps.RW;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.adapter.AdapterLatestRingtone;
import com.apps.items.ItemRingtone;
import com.apps.utils.Constant;
import com.apps.utils.EndlessRecyclerViewScrollListener;
import com.apps.utils.JsonUtils;
import com.apps.utils.Methods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ByCatRingActivity extends AppCompatActivity {

    Methods methods;
    Toolbar toolbar;
    RecyclerView recyclerView;
    FragmentManager fm;
    LinearLayoutManager layout;
    AdapterLatestRingtone adapterLatestRingtone;
    ArrayList<ItemRingtone> arrayList_ringtone;
    ProgressDialog progressDialog;
    String cid = "", cname = "", json;
    TextView textView_empty;
    SearchView searchView;
    Boolean isOver = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_by_cat_ring);

        methods = new Methods(this);
        toolbar = (Toolbar)findViewById(R.id.toolbar_ring_bycat);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        methods.setStatusColor(getWindow(),toolbar);
        methods.forceRTLIfSupported(getWindow());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);

        cid = getIntent().getStringExtra("cid");
        cname = getIntent().getStringExtra("cname");
        getSupportActionBar().setTitle(cname);

        textView_empty = (TextView)findViewById(R.id.tv_cat_ringwall);

//        loadInter();

        arrayList_ringtone = new ArrayList<>();
        adapterLatestRingtone = new AdapterLatestRingtone(this,arrayList_ringtone);

        layout = new LinearLayoutManager(this);
        recyclerView = (RecyclerView)findViewById(R.id.rv_cat_ring);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layout);

        if(methods.isNetworkAvailable()) {
            new LoadRing().execute();
        } else {
            Toast.makeText(this, getResources().getString(R.string.net_not_conn), Toast.LENGTH_SHORT).show();
        }

//        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
////				if((position+1)%4!=0) {
////					showInter(position);
////				}
////                showInter(position);
//                Constant.arrayList_ring.clear();
//                Constant.arrayList_ring.addAll(arrayList_ringtone);
//                Intent intent = new Intent(ByCatRingActivity.this,DetailRingtone.class);
//                intent.putExtra("pos",position);
//                startActivity(intent);
//            }
//        }));

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layout) {
            @Override
            public void onLoadMore(int p, int totalItemsCount) {
                if(!isOver) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new LoadRing().execute();
                        }
                    }, 2000);
                } else {
                    Toast.makeText(ByCatRingActivity.this, getResources().getString(R.string.no_more_data), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onCreateOptionsMenu(menu);
    }

    SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {

            if (searchView.isIconified()) {
                recyclerView.setAdapter(adapterLatestRingtone);
                adapterLatestRingtone.notifyDataSetChanged();
            } else {
                adapterLatestRingtone.getFilter().filter(s);
                adapterLatestRingtone.notifyDataSetChanged();
            }
            return true;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoadRing extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            if (arrayList_ringtone.size() == 0) {
                progressDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                if(arrayList_ringtone.size() == 0) {
                    json = JsonUtils.getJSONString(Constant.URL_RING_BY_CAT + cid);
                }
                JSONObject mainJson = new JSONObject(json);
                JSONArray jsonArray = mainJson.getJSONArray(Constant.TAG_ROOT);

                if(jsonArray.length() <= arrayList_ringtone.size()+Constant.dataLoadNumber) {
                    isOver = true;
                }

                int a = arrayList_ringtone.size();
                int b = a + Constant.dataLoadNumber;

                for (int i = a; i < b; i++) {
                    JSONObject objJson = jsonArray.getJSONObject(i);

                    String id = objJson.getString(Constant.TAG_ID);
                    String cid = objJson.getString(Constant.TAG_CAT_ID);
                    String user_name = objJson.getString(Constant.TAG_USER_NAME);
                    String cname = objJson.getString(Constant.TAG_CAT_NAME);
                    String type = objJson.getString(Constant.TAG_RINTONE_TYPE);
                    String title = objJson.getString(Constant.TAG_RINTONE_TITLE);
                    String url = objJson.getString(Constant.TAG_RINTONE_URL);
                    String duration = objJson.getString(Constant.TAG_RINTONE_DURATION);
                    String image = objJson.getString(Constant.TAG_RINTONE_IMAGE_B).replace(" ","%20");
                    String image_small = objJson.getString(Constant.TAG_RINTONE_IMAGE_S).replace(" ","%20");
                    String tags = objJson.getString(Constant.TAG_TAGS);
                    String rate_avg = objJson.getString(Constant.TAG_RATE_AVG);
                    if(rate_avg.equals("")) {
                        rate_avg = "0";
                    }
//                    String total_rate = objJson.getString(Constant.TAG_TOTAL_RATE);
                    String total_views = objJson.getString(Constant.TAG_TOTAL_VIEWS);
                    String total_download = objJson.getString(Constant.TAG_TOTAL_DOWNLOADS);

                //    ItemRingtone itemRingtone = new ItemRingtone(vid, username, id,user_name,type,title,url,duration,image,image_small,rate_avg, total_views,total_download);
                 //   arrayList_ringtone.add(itemRingtone);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                isOver = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if(arrayList_ringtone.size()<(Constant.dataLoadNumber+1)) {
                adapterLatestRingtone = new AdapterLatestRingtone(ByCatRingActivity.this,arrayList_ringtone);
                recyclerView.setAdapter(adapterLatestRingtone);
            } else {
                adapterLatestRingtone.notifyDataSetChanged();
            }

            if(arrayList_ringtone.size() == 0) {
                textView_empty.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                textView_empty.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

        }
    }
}
